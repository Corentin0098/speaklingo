<?php

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\XSS;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::namespace('App\Http\Controllers')->group(function () {
    Route::group(['middleware' => ['XSS']], function () {

        // Main page
        Route::middleware(['guest'])->group(function () {
        Route::get('/', 'HomeController@home')->name('home');
        });

        /*--------------------------------------------------------------------------
        | USERCONTROLLER
        --------------------------------------------------------------------------*/
        // Register
        Route::middleware(['guest'])->group(function () {
            Route::get('register', 'UserController@register')->name('register');
            Route::post('createRegister', 'UserController@createRegister')->name('createRegister');
        });
        Route::middleware(['guest'])->group(function () {
            Route::middleware(['recently.registered'])->group(function () {
                Route::get('success_register', 'UserController@successRegister')->name('success_register');
            });
            Route::get('expired_register/{token}', 'UserController@expiredRegister')->name('expired_register');
            Route::post('resend-verification-link', 'UserController@resendVerificationLink')->name('resend-verification-link');
        });

        // Login
        Route::middleware(['guest'])->group(function () {
            Route::get('login', 'UserController@login')->name('login');
            Route::post('login', 'UserController@connectToProfile')->name('loginPost');
            Route::get('verify-email/{token}', 'UserController@verifyEmail')->name('verify-email');

            // reset password
            Route::get('reset', 'UserController@resetPassword')->name('reset');

            Route::post('password/forgot', 'ForgotPasswordController@sendResetLinkEmail');
            Route::post('password/reset', 'ResetPasswordController@reset');
            Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        });

        // IF AUTHENTICATED
        Route::middleware(['auth'])->group(function () {
            // Profil
            Route::get('profile', 'UserController@profile')->name('profile');
            Route::get('dashboard', 'DashboardController@dashboard')->name('dashboard');

            // Setting
            Route::middleware(['not_admin'])->group(function () {
                Route::get('setting', 'SettingController@setting')->name('setting');
                Route::post('updateProfile', 'SettingController@updateProfile')->name('update.profile');
                Route::post('updatePassword', 'SettingController@updatePassword')->name('update.password');
                Route::post('verifyPassword', 'SettingController@verifyPassword')->name('verify.password');
                Route::post('updateProfilePhoto', 'SettingController@updateProfilePhoto')->name('update.profile-photo');

                // Payment
                Route::get('payment', 'StripeController@payment')->name('payment');
                Route::get('payment_success', 'StripeController@success')->name('payment_success');

                Route::post('checkout', 'StripeController@checkout')->name('checkout');
                Route::get('success', 'StripeController@success')->name('success');

                // Calendar
                Route::get('calendar', 'MeetingController@calendar')->name('calendar');

                // Lesson History
                Route::get('lesson', 'LessonController@lesson')->name('lesson');
                Route::get('exchange-rate', 'ExchangeRateController@getExchangeRate');
            });

            Route::middleware(['not_student'])->group(function () {
                Route::get('get-allowed-dates-teacher', 'BookingController@getAllowedDatesTeacher');
                Route::get('get-allowed-times-teacher', 'BookingController@getAllowedTimesTeacher');
                Route::delete('delete-scheduled-time/{date}/{time}', 'BookingController@deleteScheduledTime');
                Route::delete('delete-scheduled-date/{date}', 'BookingController@deleteScheduledDate');
                Route::get('get-all-events', 'BookingController@getAllEvents');
                Route::post('add-date-from-calendar-teacher', 'BookingController@addDateFromCalendarTeacher');
                Route::post('event-update', 'BookingController@eventUpdate');
                Route::post('update-price', 'DashboardController@updatePrice');
            });

            // Video Chat
            Route::get('meeting', 'MeetingController@meeting')->name('meeting');

            Route::post('add-date', 'BookingController@addDate');
            Route::get('get-allowed-dates', 'BookingController@getAllowedDates');
            Route::get('get-allowed-times', 'BookingController@getAllowedTimes');

            // Booking
            Route::resource('booking', 'BookingController');

            // Update users
            Route::put('users/{id}', 'DashboardController@update');
            // Delete users
            Route::delete('users/{id}', 'DashboardController@softDelete');
        });

        // Logout
        Route::get('logout', 'UserController@logout')->name('logout');

        // 404 Page
        Route::fallback(function () {
            return view('Page404');
        });
    });
});

// Change local language Laravel
Route::post('language', function (Illuminate\Http\Request $request) {
    $locale = $request->input('locale');

    if (in_array($locale, ['en', 'fr'])) {
        session(['locale' => $locale]);

        return response()->json(['locale' => $locale])->withCookie(cookie('user_locale', $locale, 60*24*365));
    } else {
        return response()->json(['error' => 'Invalid language code'], 400);
    }
});

