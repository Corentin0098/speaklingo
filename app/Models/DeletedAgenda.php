<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeletedAgenda extends Model
{
    use HasFactory;
    protected $table = 'deleted_agendas';
    protected $fillable = ['user_id', 'name', 'start_time', 'end_time'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
