<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $primaryKey = 'country_id';

    protected $fillable = [
        'country_name',
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'country_id');
    }
}
