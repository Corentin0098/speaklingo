<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone',
        'country_id',
        'password',
        'terms',
        'last_login',
        'email_verified',
        'verification_token',
        'verification_token_expires_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    use SoftDeletes;

    protected $primaryKey = 'id';

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }

    public function hasRole($roleName)
    {
        return $this->roles->contains('role_name', $roleName);
    }

    public function messages()
    {
        return $this->hasMany(ChMessage::class, 'id');
    }
    public function purchases()
    {
        return $this->hasMany(Purchase::class, 'id');
    }

    public function agenda()
    {
        return $this->hasMany(Agenda::class);
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class, 'id', 'id');
    }
}
