<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'transaction_id',
        'amount',
        'currency',
        'credits_earned'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function move()
    {
        return $this->belongsTo(Move::class, 'move_id');
    }
}
