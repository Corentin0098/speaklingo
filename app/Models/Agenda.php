<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'name',
        'start_time',
        'end_time',
        'google_event_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getAppointmentsForDate($date)
    {
        return $this->whereDate('start_time', $date)->get();
    }
}
