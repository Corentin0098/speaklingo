<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class VerificationMail extends Mailable
{
    protected $user;
    protected $verificationToken;

    public function __construct(User $user, $verificationToken)
    {
        $this->user = $user;
        $this->verificationToken = $verificationToken;
    }

    public function build()
    {
        $verificationUrl = URL::temporarySignedRoute(
            'verify-email',
            now(),
            ['token' => $this->verificationToken]
        );

        return $this->subject('Email address verification')
            ->view('verification')
            ->with(['user' => $this->user, 'verificationUrl' => $verificationUrl])
            ->withHeaders([
                'X-Message-Delivery' => 'full',
            ]);
    }
}

