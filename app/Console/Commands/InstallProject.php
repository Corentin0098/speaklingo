<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InstallProject extends Command
{
    protected $signature = 'install:project';

    protected $description = 'Install the project and update the .env file';

    public function handle()
    {
        // Copie le fichier .env.example en tant que .env
        File::copy('.env.example', '.env');

        // Génère une nouvelle clé APP_KEY
        $this->call('key:generate');

        // Demander les informations du serveur
        $dbHost = $this->ask('Enter the database host (Enter for default)', '127.0.0.1');
        $dbPort = $this->ask('Enter the database port (Enter for default)', '3306');
        $dbDatabase = $this->ask('Enter the name of the database (Exemple: project_web)');
        $dbUsername = $this->ask('Enter the database username (Enter for default)', 'root');
        $dbPassword = $this->secret('Enter the database password (default: empty)');

        // Charge le contenu du fichier .env
        $envContents = File::get('.env');

        // Définir les lignes à supprimer
        $linesToRemove = [
            'PUSHER_HOST=',
            'PUSHER_PORT=443',
            'PUSHER_SCHEME=https'
        ];

        // Supprimer les lignes spécifiques du fichier .env
        foreach ($linesToRemove as $line) {
            $envContents = preg_replace("/" . preg_quote($line, "/") . ".*\n/", "", $envContents);
        }

        // Met à jour le fichier .env avec les informations récupérées
        $envContents = preg_replace([
            '/APP_DEBUG=(.*)/',
            //Pusher
            '/BROADCAST_DRIVER=(.*)/',
            '/PUSHER_APP_ID=(.*)/',
            '/PUSHER_APP_KEY=(.*)/',
            '/PUSHER_APP_SECRET=(.*)/',
            '/PUSHER_APP_CLUSTER=(.*)/',

            '/DB_HOST=(.*)/',
            '/DB_PORT=(.*)/',
            '/DB_DATABASE=(.*)/',
            '/DB_USERNAME=(.*)/',
            '/DB_PASSWORD=(.*)/'
        ], [
            'APP_DEBUG=false',
            //Pusher
            'BROADCAST_DRIVER=pusher',
            'PUSHER_APP_ID=1725438',
            'PUSHER_APP_KEY=c2739f2a5446cd747e8f',
            'PUSHER_APP_SECRET=58a02e1cb67db5a09092',
            'PUSHER_APP_CLUSTER=eu',

            'DB_HOST=' . $dbHost,
            'DB_PORT=' . $dbPort,
            'DB_DATABASE=' . $dbDatabase,
            'DB_USERNAME=' . $dbUsername,
            'DB_PASSWORD=' . $dbPassword
        ], $envContents);

        // Met les modifications dans le fichier .env
        File::put('.env', $envContents);

        $this->info('Laravel project has been installed !');
    }
}
