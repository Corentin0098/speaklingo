<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Role;
use App\Models\ChMessage;
use App\Models\Agenda;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use Illuminate\Support\Str;
use App\Models\Booking;
use Spatie\GoogleCalendar\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CreateUserWithMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:users {count} {messages_per_user}';

    protected $description = 'Create multiple random users with messages between the users and admin';

    public function handle()
    {
        $amounts = ['15', '30', '60'];
        $amount = $amounts[array_rand($amounts)];

        $creditsEarned = 0;
        if ($amount == '15') {
            $creditsEarned = 1;
        } elseif ($amount == '30') {
            $creditsEarned = 2;
        } elseif ($amount == '60') {
            $creditsEarned = 4;
        }

        $count = (int) $this->argument('count');
        $messagesPerUser = (int) $this->argument('messages_per_user');

        if ($count <= 0 || $messagesPerUser <= 0) {
            $this->error('Please provide valid counts greater than 0.');
            return;
        }

        $startDate = Carbon::createFromDate(now()->year, 1, 1);
        $endDate = Carbon::now();

        for ($i = 0; $i < $count; $i++) {

            $emailVerified = Faker::create()->boolean;

            $data = [
                'firstname' => Faker::create()->firstName,
                'lastname' => Faker::create()->lastName,
                'email' => Faker::create()->unique()->safeEmail,
                'phone' => Faker::create()->phoneNumber,
                'country_id' => Faker::create()->numberBetween(1, 20),
                'password' => Hash::make('password'),
                'terms' => 1,
                'email_verified' => $emailVerified,
                'credits' => $emailVerified ? Faker::create()->numberBetween(0, 5) : 0,
                'last_login' => $emailVerified ? now()->subHours(Faker::create()->numberBetween(0, 48)) : null,
            ];

            $user = User::create($data);

            $studentRole = Role::firstOrCreate(['role_name' => 'student']);
            $user->roles()->attach($studentRole);

            if ($emailVerified) {
                $user->credits = $data['credits'];
                $user->save();

                $this->startConversation($user, $messagesPerUser);

                $transactionDate = $this->generateRandomDate($startDate, $endDate);

                DB::table('purchases')->insert([
                    'user_id' => $user->id,
                    'transaction_id' => Str::random(20),
                    'amount' => $amount,
                    'currency' => 'eur',
                    'credits_earned' => $creditsEarned,
                    'created_at' => $transactionDate,
                    'updated_at' => $transactionDate,
                ]);

                $this->createAppointment($user);

                $this->info('User ' . ($i + 1) . ' created successfully with a conversation with the admin and appointment.');
            } else {
                $this->info('User ' . ($i + 1) . ' created with email not verified. No messages or appointments created.');
            }
        }
    }

    private function startConversation($user, $count)
    {
        $adminRole = Role::where('role_name', 'admin')->first();
        $adminUser = $adminRole->users()->first();

        if (!$user->email_verified) {
            return;
        }

        $users = User::where('id', '!=', $adminUser->id)
            ->inRandomOrder()
            ->limit(10)
            ->get();

        foreach ($users as $sender) {
            for ($i = 0; $i < $count; $i++) {
                $userMessage = new ChMessage();
                $userMessage->from_id = $sender->id;
                $userMessage->to_id = $adminUser->id;
                $userMessage->body = Faker::create()->sentence;
                $userMessage->save();

                $adminMessage = new ChMessage();
                $adminMessage->from_id = $adminUser->id;
                $adminMessage->to_id = $sender->id;
                $adminMessage->body = Faker::create()->sentence;
                $adminMessage->save();
            }
        }
    }

    private function createAppointment($user)
    {
        $faker = Faker::create();

        $startDate = Carbon::now()->subDays(30);
        $endDate = Carbon::now()->addDays(30);

        $appointmentsPerUser = random_int(1, 4);

        for ($i = 0; $i < $appointmentsPerUser; $i++) {
            $hour = $faker->numberBetween(0, 23);

            $start_time = $this->generateRandomDate($startDate, $endDate)->hour($hour)->minute(0)->second(0);
            $end_time = $start_time->copy()->addMinutes(50);

            // Check for conflicts
            $conflict = Agenda::where('user_id', $user->id)
                ->where(function($query) use ($start_time, $end_time) {
                    $query->whereBetween('start_time', [$start_time, $end_time])
                        ->orWhereBetween('end_time', [$start_time, $end_time])
                        ->orWhere(function($query) use ($start_time, $end_time) {
                            $query->where('start_time', '<=', $start_time)
                                ->where('end_time', '>=', $end_time);
                        });
                })
                ->exists();

            if (!$conflict) {
                // Add event to Google Calendar
                $event = new Event();
                $event->name = $user->firstname . ' ' . $user->lastname;
                $event->startDateTime = $start_time;
                $event->endDateTime = $end_time;
                $eventId = $event->save()->googleEvent->id;

                Agenda::create([
                    'user_id' => $user->id,
                    'name' => $user->firstname . ' ' . $user->lastname,
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'google_event_id' => $eventId,
                ]);

                $this->info('Appointment created for user ' . $user->id);
            } else {
                $this->error('Conflict detected, appointment not created for user ' . $user->id);
            }
        }
    }

    private function generateRandomDate($startDate, $endDate)
    {
        $randomTimestamp = mt_rand($startDate->timestamp, $endDate->timestamp);
        return Carbon::createFromTimestamp($randomTimestamp);
    }
}
