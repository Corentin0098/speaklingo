<?php

namespace App\Console\Commands;

use App\Models\Country;
use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CreateAdmin extends Command
{
    protected $signature = 'create:admin';
    protected $description = 'Create a new admin user';

    public function handle()
    {
        // Checking if an admin already exists
        if (User::whereHas('roles', function ($query) {
            $query->where('role_name', 'admin');
        })->exists()) {
            $this->info('An admin user already exists. Cannot create another admin.');
            return;
        }

        $isValid = false;

        while (!$isValid) {
            // Asking to give the inputs
            $firstname = $this->ask('Enter the administrator\'s firstname (min:3)');
            $lastname = $this->ask('Enter the administrator\'s lastname (min:3)');
            $email = $this->ask('Enter the administrator\'s email');
            $countries = Country::pluck('country_name', 'country_id')->toArray();
            $country = $this->choice('Select the administrator\'s country:', $countries);
            $password = $this->secret('Enter the administrator\'s password (min:9 + min 1 maj)');

            // Validation rules
            $validator = Validator::make([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'country' => $country,
                'password' => $password,
            ], [
                'firstname' => 'required|string|min:3|max:255',
                'lastname' => 'required|string|min:3|max:255',
                'email' => 'required|email|unique:users|max:255',
                'country' => 'required|exists:countries,country_name',
                'password' => 'required|string|min:9|max:255|regex:/^(?=.*[A-Z])(?=.*\d).+$/',
            ]);

            // If inputs are incorrect
            if ($validator->fails()) {
                $this->error('Validation failed. Please correct the following errors:');
                foreach ($validator->errors()->all() as $error) {
                    $this->error('- ' . $error);
                }
            } else {
                $isValid = true;
            }
        }

        $countryId = Country::where('country_name', $country)->value('country_id');

        $user = User::create([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'phone' => '+0000000000',
            'country_id' => $countryId,
            'password' => Hash::make($password),
            'terms' => true,
            'email_verified' => true,
        ]);

        $user->save();

        // Give the admin role
        $adminRole = Role::firstOrCreate(['role_name' => 'admin']);
        $user->roles()->attach($adminRole);

        $this->info('The administrator has been created!');
    }
}
