<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateLastLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if (Auth::check()) {
            // Vérifier si le champ à mettre à jour est 'last_login'
            $fieldsToUpdate = array_keys($request->all());
            if (in_array('last_login', $fieldsToUpdate)) {
                Auth::user()->update(['last_login' => now()]);
            }
        }

        return $response;
    }
}
