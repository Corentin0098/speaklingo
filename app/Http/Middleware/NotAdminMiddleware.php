<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotAdminMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            $adminRole = 'admin';
            if (Auth::user()->hasRole($adminRole)) {
                return redirect()->route('dashboard');
            }
        }

        return $next($request);
    }
}
