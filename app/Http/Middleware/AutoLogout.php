<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AutoLogout
{
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $last_login = auth()->user()->last_login;

            // Durée d'inactivité avant la déconnexion (en minutes)
            $inactiveDuration = config('auth.auto_logout_duration');

            if (time() - strtotime($last_login) > $inactiveDuration * 60) {
                Auth::logout();
                return redirect('/logout');
            }
        }

        return $next($request);
    }
}
