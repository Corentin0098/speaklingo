<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                // Si l'utilisateur est un admin, redirigez-le vers le dashboard
                $adminRole = 'admin';
                if (Auth::user()->hasRole($adminRole)) {
                    return redirect()->route('dashboard');
                } else {
                    $request->session()->put('user_id', Auth::user()->id);

                    return redirect()->route('profile');
                }
            }
        }

        return $next($request);
    }
}
