<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LanguageDetection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->input('locale') ?? $request->cookie('user_locale');

        if ($locale && in_array($locale, ['en', 'fr'])) {
            app()->setLocale($locale);
        }

        return $next($request);
    }
}
