<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RecentlyRegistered
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->has('registered_user_id')) {
            return $next($request);
        } else {
            return redirect('/register')->with('error', 'Inscription requise.');
        }
    }
}
