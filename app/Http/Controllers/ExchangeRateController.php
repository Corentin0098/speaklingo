<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ExchangeRateController extends Controller
{
    public function getExchangeRate()
    {
        $apiKey = config('stripe.exchange_rate');

        $response = Http::get("https://v6.exchangerate-api.com/v6/{$apiKey}/latest/EUR");

        if ($response->successful()) {
            $exchangeRateData = $response->json();
            return response()->json($exchangeRateData);
        } else {
            return response()->json(['error' => 'Failed to fetch exchange rate data'], $response->status());
        }
    }
}
