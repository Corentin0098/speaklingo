<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Models\Agenda;
use App\Models\ChMessage;
use App\Models\Price;
use App\Models\Purchase;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function dashboard() {

        $user = Auth::user();

        if ($user->hasRole('student')) {
            return redirect()->route('profile');
        }

        $users = User::all();
        $messages = ChMessage::all();
        $user = Auth::user();
        $appointments = $user->agenda();
        $purchases = Purchase::all();
        $agendas = Agenda::all();
        $price = Price::all();

        if (!$user) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        return view('dashboard', [
            'users' => $users,
            'user' => $user,
            'appointments' => $appointments,
            'messages' => $messages,
            'purchases' => $purchases,
            'agendas' => $agendas,
            'prices' => $price,
        ]);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        // Find the user by ID
        $user = User::findOrFail($id);

        // Update user data with validated data from the request
        $user->update($request->validated());

        return response()->json([
            'message' => 'User updated successfully',
            'user' => $user,
        ]);
    }

    public function softDelete($id)
    {
        $user = User::findOrFail($id);

        ChMessage::where('from_id', $id)
            ->orWhere('to_id', $id)
            ->delete();

        Agenda::where('user_id', $id)
            ->delete();

        // Blur user's datas
        $user->firstname = 'Anonyme';
        $user->lastname = 'Anonyme';
        $user->email = 'anonyme' . $id . '@example.com';
        $user->phone = '+000000000' . $id;
        $user->country_id = 1;
        $user->terms = 0;
        $user->credits = 0;
        $user->last_login = null;
        $user->avatar = '';

        $user->save();

        $user->delete();

        return response()->json(['message' => 'Utilisateur supprimé et brouillé'], 200);
    }

    public function updatePrice(Request $request) {
        // Récupérer les données de la requête
        $priceId = $request->input('price_id');
        $newPrice = $request->input('price');

        try {
            // Trouver le prix en base de données
            $price = Price::findOrFail($priceId);

            // Mettre à jour le montant du prix
            $price->value = $newPrice;
            $price->save();

            // Réponse JSON en cas de succès
            return response()->json(['message' => 'Price updated successfully'], 200);
        } catch (\Exception $e) {
            // Réponse JSON en cas d'erreur
            return response()->json(['message' => 'Error updating price', 'error' => $e->getMessage()], 500);
        }
    }
}
