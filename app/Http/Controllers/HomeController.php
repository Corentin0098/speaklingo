<?php

namespace App\Http\Controllers;

use App\Models\Price;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {
        $price = Price::all();

        return view('home', [
            'prices' => $price,
        ]);
    }
}
