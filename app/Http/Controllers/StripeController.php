<?php

namespace App\Http\Controllers;

use App\Models\ChMessage;
use App\Models\Price;
use App\Models\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class StripeController extends Controller
{
    public function payment(Request $request) {
        $user = $request->user();
        $messages = ChMessage::all();
        $country = DB::table('countries')->where('country_id', $user->country_id)->first();
        $price = Price::all();

        return view('payment', [
            'user' => $user,
            'country' => $country,
            'messages' => $messages,
            'prices' => $price,
        ]);
    }

    public function checkout(Request $request) {
        \Stripe\Stripe::setApiKey(config('stripe.sk'));

        $user = $request->user();
        $country = DB::table('countries')->where('country_id', $user->country_id)->first();
        $locale = $request->input('parentSelectedLanguage');
        $localeShort = $locale === 'Français' ? 'fr' : 'en';

        $defaultCurrency = 'EUR';
        $defaultAmount = $request->input('amount');

        $productName = $locale === 'Français' ? 'Cours' : 'Lesson';

        $client = new Client();
        $apiKey = config('stripe.exchange_rate');

        $response = $client->get("https://v6.exchangerate-api.com/v6/{$apiKey}/latest/EUR");
        $rates = json_decode($response->getBody(), true);

        $countryCurrencies = include(config_path('countryCurrencies.php'));
        $userCurrency = isset($countryCurrencies[$country->code_iso]) ? strtoupper($countryCurrencies[$country->code_iso]) : $defaultCurrency;

        if (!isset($rates['conversion_rates'][$userCurrency])) {
            return response()->json(['error' => 'Le taux de conversion n\'est pas disponible pour la devise ' . $userCurrency], 400);
        }

        $conversionRate = $rates['conversion_rates'][$userCurrency];

        $amountInUserCurrency = $defaultAmount * $conversionRate;

        $session = \Stripe\Checkout\Session::create([
                'line_items' => [
                    [
                        'price_data' => [
                            'currency' => $userCurrency,
                            'product_data' => [
                                'name' => $productName,
                            ],
                            'unit_amount' => round($amountInUserCurrency),
                        ],
                        'quantity' => 1,
                    ],
                ],
                'mode' => 'payment',
                'success_url' => route('success'),
                'cancel_url' => route('home'),
                'locale' => $localeShort,
            ]);

        $request->session()->put('checkout_session_id', $session->id);

        if ($request->expectsJson()) {
            return response()->json(['redirect_url' => $session->url]);
        } else {
            return redirect()->away($session->url);
        }
    }

    public function success(Request $request) {

        if (!$request->session()->has('checkout_session_id')) {
            return redirect()->route('home');
        }

        $user = $request->user();

        $sessionId = $request->session()->pull('checkout_session_id');

        \Stripe\Stripe::setApiKey(config('stripe.sk'));
        $stripeSession = \Stripe\Checkout\Session::retrieve($sessionId);

        if ($stripeSession->payment_status === 'paid') {
            $amount = $stripeSession->amount_total / 100;
            $currency = $stripeSession->currency;

            $convertedAmount = $amount;
            if ($currency !== 'EUR') {
                $client = new Client();
                $apiKey = config('stripe.exchange_rate');

                $response = $client->get("https://v6.exchangerate-api.com/v6/{$apiKey}/pair/{$currency}/EUR");
                $data = json_decode($response->getBody(), true);

                if (isset($data['conversion_rate'])) {
                    $convertedAmount *= $data['conversion_rate'];
                } else {
                    return response()->json(['error' => 'Le taux de conversion n\'est pas disponible pour la devise ' . $currency], 400);
                }
            }

            $roundedAmount = round($convertedAmount);

            $creditsEarned = 0;
            switch ($roundedAmount) {
                case 15:
                    $creditsEarned = 1;
                    break;
                case 30:
                    $creditsEarned = 2;
                    break;
                case 60:
                    $creditsEarned = 4;
                    break;
                default:
                    break;
            }

            // Enregistrer la transaction de paiement
            $paymentTransaction = new Purchase();
            $paymentTransaction->user_id = $user->id;
            $paymentTransaction->transaction_id = $stripeSession->id;
            $paymentTransaction->amount = $amount;
            $paymentTransaction->currency = $currency;
            $paymentTransaction->credits_earned = $creditsEarned;
            $paymentTransaction->save();

            $user->credits += $creditsEarned;
            $user->save();
        }

        return view('payment_success');
    }
}
