<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\ChMessage;
use App\Models\Country;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerificationMail;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Profiler\Profile;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    // REGISTER
    public function register(){
        return view('register');
    }
    public function successRegister() {
        return view('successRegister');
    }

    public function expiredRegister($token) {
        $user = User::where('verification_token', $token)->first();

        if (!$user) {
            return redirect('/')->with('error', 'Le token est invalide.');
        }

        if (Carbon::now()->greaterThan($user->verification_token_expires_at)) {
            return view('expiredRegister');
        }

        return redirect()->route('success_register', ['token' => $token]);
    }
    public function createRegister(RegisterRequest $request) {

        if (Auth::check()) {
            return Redirect::to('profile');
        }

        $data = $request->validated();

        $user = User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'country_id' => $data['country_id'],
            'password' => Hash::make($data['password']),
            'terms' => $data['terms'],
            'verification_token' => Str::random(60),
            'verification_token_expires_at' => Carbon::now()->addMinutes(10),
        ]);

        $request->session()->put('registered_user_id', $user->id);

        $user->save();

        $request->session()->put('registered_user_id', $user->id);

        Mail::to($user->email)->send(new VerificationMail($user, $user->verification_token));

        // Give "student" role by default
        $studentRole = Role::firstOrCreate(['role_name' => 'student']);
        $user->roles()->attach($studentRole);

        $this->sendAdminMessage($user);

        return Redirect::to('successRegister');
    }

    public function verifyEmail($token)
    {
        $user = User::where('verification_token', $token)->first();

        if (!$user) {
            return redirect()->route('login')->with('email_verify_error', true);
        } elseif ($user->email_verified) {
            return redirect()->route('login')->with('email_already_verified', true);
        }

        if (Carbon::now()->greaterThan($user->verification_token_expires_at)) {
            return view('expiredRegister');
        }

        $user->email_verified = true;
        $user->verification_token = null;
        $user->verification_token_expires_at = null;
        $user->save();

        return redirect()->route('login')->with('email_verified_success', true);
    }

    public function resendVerificationLink(Request $request) {
        $email = $request->input('email');
        $user = User::where('email', $email)->first();

        if (!$user) {
            return response()->json(['error' => 'User not found'], 404);
        }

        if ($user->email_verified) {
            return response()->json(['error' => 'Email already verified'], 409);
        }

        if (Carbon::now()->lessThanOrEqualTo($user->verification_token_expires_at)) {
            return response()->json(['error' => 'Token has not yet expired'], 409);
        }

        $newToken = Str::random(60);
        $user->verification_token = $newToken;
        $user->verification_token_expires_at = Carbon::now()->addMinutes(10);
        $user->save();

        Mail::to($user->email)->send(new VerificationMail($user, $newToken));

        return response()->json(['success' => 'A new verification link has been sent.']);
    }

    private function sendAdminMessage($user)
    {
        $adminRole = Role::where('role_name', 'admin')->first();
        $adminUser = $adminRole->users()->first();

        // Send a message to the student
        $adminMessage = new ChMessage();
        $adminMessage->from_id = $adminUser->id;
        $adminMessage->to_id = $user->id;
        $adminMessage->body = "Welcome! I would love to help you improve your English. Feel free to let me know your goals and interests or ask me any questions.";

        $adminMessage->save();
    }

    // LOGIN
    public function login(){
        return view('login');
    }

    public function resetPassword(){
        return view('reset');
    }

    public function connectToProfile(LoginRequest $request) {

        $data = $request->validated();

        $request->session()->regenerate();

        $user = User::where('email', $data['email'])->first();

        if (!$user) {
            return response()->json(['error' => 'Invalid email or password'], 422);
        }

        if (!$user->email_verified) {
            return response()->json(['error' => 'Email not verified'], 403);
        }

        if (Auth::attempt($data)) {

            $user->update([
                'last_login' => now(),
            ]);

            return response()->json(['authenticated' => true, 'user' => Auth::user()]);
        }

        return response()->json(['error' => 'Invalid email or password'], 422);
    }

    // PROFILE
    public function profile() {

        $user = Auth::user();

        if ($user->hasRole('admin')) {
            return redirect()->route('dashboard');
        }

        $users = User::all();
        $messages = ChMessage::all();
        $user = Auth::user();
        $appointments = $user->agenda();
        $country = DB::table('countries')->where('country_id', $user->country_id)->first();

        if (!$user) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        return view('profile', [
            'users' => $users,
            'user' => $user,
            'appointments' => $appointments,
            'messages' => $messages,
            'country' => $country,
        ]);
    }

    // PROFILE
    public function logout(Request $request)
    {
        auth()->logout();
        Session()->flush();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }
}
