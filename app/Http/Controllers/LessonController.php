<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use App\Models\ChMessage;
use App\Models\DeletedAgenda;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LessonController extends Controller
{
    public function lesson() {

        $user = Auth::user();
        $country = DB::table('countries')->where('country_id', $user->country_id)->first();

        if ($user->hasRole('admin')) {
            return redirect()->route('dashboard');
        }

        $messages = ChMessage::all();
        $appointments = Agenda::where('user_id', $user->id)->get();
        $canceledAppointments = DeletedAgenda::where('user_id', $user->id)->get();

        $appointments = $appointments->map(function($appointment) {
            $appointment->is_canceled = false;
            return $appointment;
        });

        $canceledAppointments = $canceledAppointments->map(function($appointment) {
            $appointment->is_canceled = true;
            return $appointment;
        });

        $allAppointments = $appointments->concat($canceledAppointments);

        if (!$user) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        return view('lesson', [
            'user' => $user,
            'appointments' => $allAppointments,
            'messages' => $messages,
            'country' => $country,
        ]);
    }
}
