<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use App\Models\UserRole;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Spatie\GoogleCalendar\Event;

class MeetingController extends Controller
{
    public function meeting() {
        Carbon::setTestNow();

        $user = Auth::user();
        $role = Role::where('role_name', 'admin')->first();
        $roleName = $role->role_name;

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        if ($user->hasRole('admin')) {
            return view('meeting', [
                'user' => $user,
                'role' => $roleName
            ]);
        }

        $meeting = $user->agenda()
            ->where('start_time', '>=', Carbon::now())
            ->orderBy('start_time', 'asc')
            ->get();

        if (!$meeting) {
            return redirect()->route('profile');
        }

        $meetings = $meeting->first();

        if (!$meetings) {
            return redirect()->route('profile');
        }

        $meetingStartTime = Carbon::parse($meetings->start_time);

        $allowedStart = $meetingStartTime->copy()->subMinutes(5);
        $allowedEnd = $meetingStartTime->copy()->addMinutes(55);

        $now = Carbon::now()->addHour(2);

        if ($now->gte($allowedStart) && $now->lte($allowedEnd)) {
            return view('meeting', [
                'user' => $user,
                'meeting' => $meeting,
                'role' => 'student'
            ]);
        }

        return redirect()->route('profile');
    }

    public function calendar() {
        $event = new Event();
        $event->name = 'book 1 lesson';
        $event->startDateTime = Carbon::now();
        $event->endDateTime = Carbon::now()->addHour();

        $event->save();

        $e = Event::get();

        return view('calendar');
    }
}
