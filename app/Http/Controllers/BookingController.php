<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use App\Models\Booking;
use App\Models\ChMessage;
use App\Models\DeletedAgenda;
use App\Models\Role;
use App\Models\User;
use AWS\CRT\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\GoogleCalendar\Event;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;


class BookingController extends Controller
{
    public function index()
    {
        if (!Auth::user()->hasRole('admin')) {
            return redirect()->route('profile');
        }
        return redirect()->route('dashboard');
    }
    public function store(Request $request)
    {
        $user = Auth::user();

        $defaultName = $user->firstname . ' ' . $user->lastname;

        if ($user->credits > 0) {
            if ($request->expectsJson()) {
                $validatedData = $request->validate([
                    'meeting_date' => 'required|date',
                    'meeting_time' => 'required|string',
                    'timezone' => 'required|string',
                ]);
                $validatedData['name'] = $defaultName;

                $userTimezone = $validatedData['timezone'];
                $startTime = Carbon::createFromFormat('Y-m-d H:i', $validatedData['meeting_date'] . ' ' . $validatedData['meeting_time'], $userTimezone)
                    ->setTimezone('UTC')->addDay()->addHours(2);

                $endTime = $startTime->copy()->addMinute(50);

                $agenda = Agenda::where('start_time', $startTime)->where('end_time', $endTime)->first();
                if ($agenda) {
                    return response()->json(['error' => 'This time is already booked'], 400);
                }

                // Save the event in Google Agenda
                $event = new Event();
                $event->name = $validatedData['name'];
                $event->startDateTime = $startTime;
                $event->endDateTime = $endTime;
                $eventId = $event->save()->googleEvent->id;

                $agenda = new Agenda();
                $agenda->user_id = $user->id;
                $agenda->name = $validatedData['name'];
                $agenda->start_time = $startTime;
                $agenda->end_time = $endTime;
                $agenda->google_event_id = $eventId;

                $agenda->save();

                $user->decrement('credits');

                return response()->json(['message' => 'Appointment booked successfully'], 200);
            } else {
                return response()->json(['error' => 'Invalid request'], 400);
            }
        } else {
            return response()->json(['error' => 'Insufficient credits'], 400);
        }
    }

    public function addDate(Request $request)
    {
        $request->validate([
            'newDate' => 'required|date',
            'times' => 'required|array',
        ]);

        $newDate = $request->input('newDate');
        $times = $request->input('times');

        $booking = Booking::where('date', $newDate)->first();

        if ($booking) {
            $existingTimes = json_decode($booking->times, true);

            $updatedTimes = array_merge($existingTimes, $times);
            $updatedTimes = array_unique($updatedTimes);

            $booking->times = json_encode(array_values($updatedTimes));
            $booking->save();
        } else {
            $booking = new Booking();
            $booking->date = $newDate;
            $booking->times = json_encode($times);
            $booking->save();
        }

        return response()->json(['message' => 'New date added successfully']);
    }

    public function addDateFromCalendarTeacher(Request $request)
    {
        if ($request->expectsJson()) {
            $validatedData = $request->validate([
                'user_id' => 'required|exists:users,id',
                'name' => 'required|string',
                'meeting_date' => 'required|date',
                'meeting_time' => 'required|string',
            ]);

            $meetingDate = Carbon::parse($validatedData['meeting_date']);
            $meetingTime = Carbon::parse($validatedData['meeting_time']);

            $startTime = $meetingDate->setTimeFrom($meetingTime)->addHours(2);
            $endTime = $startTime->copy()->addMinute(50);

            $existingEvent = Agenda::where('start_time', '<', $endTime)
                ->where('end_time', '>', $startTime)
                ->first();

            if ($existingEvent) {
                return response()->json(['error' => 'Another event already exists at this time'], 400);
            }

            // Save the event in Google Agenda
            $event = new Event();
            $event->name = $validatedData['name'];
            $event->startDateTime = $startTime;
            $event->endDateTime = $endTime;
            $eventId = $event->save()->googleEvent->id;

            // Save in DB
            $agenda = new Agenda();
            $agenda->user_id = $validatedData['user_id'];
            $agenda->name = $validatedData['name'];
            $agenda->start_time = $startTime;
            $agenda->end_time = $endTime;
            $agenda->google_event_id = $eventId;

            $agenda->save();

            $user = User::find($validatedData['user_id']);
            $user->decrement('credits');

            $events = [
                'title' => $agenda->name,
                'start' => $startTime->toDateTimeString(),
                'end' => $endTime->toDateTimeString(),
            ];

            $meetingDate = $validatedData['meeting_date'];
            $meetingTime = Carbon::parse($validatedData['meeting_time'])->addHours(2)->format('H:i');

            $booking = Booking::firstOrNew(['date' => $meetingDate]);
            $times = json_decode($booking->times, true) ?: [];

            if (!in_array($meetingTime, $times)) {
                $times[] = $meetingTime;
                $booking->times = json_encode($times);
                $booking->save();
                if (empty($meetingTime)) {
                    $booking->delete();
                }
            }

            $user = $agenda->user;

            $date = Carbon::parse($validatedData['meeting_date']);
            $time = Carbon::parse($validatedData['meeting_time'])->addHours(2);

            $this->sendCreateMessage($user, $date, $time);

            return response()->json($events);
        } else {
            return response()->json(['error' => 'Invalid request'], 400);
        }
    }
    public function eventUpdate(Request $request)
    {
        $eventData = $request->input('event');
        $originalData = $request->input('original');

        $newStartTime = Carbon::parse($eventData['start'])->addHours(2);
        $newEndTime = Carbon::parse($eventData['end'])->addHours(2);

        $originalStartTime = Carbon::parse($originalData['start'])->addHours(2);

        $updatedRows = Agenda::where('start_time', '=', $originalStartTime)
            ->update([
                'start_time' => $newStartTime,
                'end_time' => $newEndTime
            ]);

        if ($updatedRows === 0) {
            return response()->json(['message' => 'Failed to update event. No matching records found or no changes made'], Response::HTTP_BAD_REQUEST);
        }

        $agenda = Agenda::where('start_time', '=', $newStartTime)->first();
        $eventId = $agenda->google_event_id;
        $event = Event::find($eventId);
        $event->update([
            'name' => $eventData['title'],
            'startDateTime' => $newStartTime->subHours(2),
            'endDateTime' => $newEndTime->subHours(2)
        ]);

        $newMeetingDate = $newStartTime->addHours(2)->format('Y-m-d');
        $oldMeetingDate = $originalStartTime->format('Y-m-d');

        $booking = Booking::firstOrNew(['date' => $newMeetingDate]);
        $times = json_decode($booking->times, true) ?: [];

        $newMeetingTime = $newStartTime->format('H:i');
        if (!in_array($newMeetingTime, $times)) {
            $times[] = $newMeetingTime;
            $booking->times = json_encode($times);
        }

        if ($booking->exists) {
            $booking->save();
        } else {
            $booking->date = $newMeetingDate;
            $booking->save();
        }

        if ($oldMeetingDate !== $newMeetingDate) {
            $oldBooking = Booking::where('date', $oldMeetingDate)->first();
            if ($oldBooking) {
                $oldTimes = json_decode($oldBooking->times, true);
                $oldMeetingTime = $originalStartTime->format('H:i');
                $oldTimes = array_diff($oldTimes, [$oldMeetingTime]);
                if (!empty($oldTimes)) {
                    $oldBooking->times = json_encode($oldTimes);
                    $oldBooking->save();
                } else {
                    $oldBooking->delete();
                }
            }
        }

        $user = $agenda->user;

        $oldStartDate = Carbon::parse($originalData['start'])->format('Y-m-d');
        $oldStartTime = Carbon::parse($originalData['start'])->addHours(2);
        $newStartDate = Carbon::parse($eventData['start'])->format('Y-m-d');
        $newStartTime = Carbon::parse($eventData['start'])->addHours(2);

        $this->sendUpdateMessage($user, $oldStartDate, $oldStartTime, $newStartDate, $newStartTime);

        return response()->json(['message' => 'Event updated successfully']);
    }

    public function getAllowedDatesTeacher()
    {
        $currentDate = now()->format('Y-m-d');
        $bookings = Booking::where('date', '>=', $currentDate)->get();
        $allowedDates = $bookings->pluck('date')->toArray();
        $earliestDate = ['1901-01-01'];
        if (empty($allowedDates)) {
            $allowedDates = $earliestDate;
        }

        return response()->json(['allowedDates' => $allowedDates]);
    }

    public function getAllowedDates(Request $request)
    {
        $request->validate([
            'timezone' => 'required|string'
        ]);

        $timezone = $request->input('timezone');

        $currentDate = now()->setTimezone('UTC')->format('Y-m-d');
        $bookings = Booking::where('date', '>=', $currentDate)->get();
        $allowedDates = [];
        $earliestDate = '1901-01-01';

        foreach ($bookings as $booking) {
            $allowedTimes = json_decode($booking->times, true);
            $agenda = Agenda::whereDate('start_time', $booking->date)->get();
            $reservedTimes = [];

            $originalDate = Carbon::parse($booking->date)->setTimezone('UTC');

            foreach ($agenda as $event) {
                $reservedTimes[] = Carbon::parse($event->start_time)->setTimezone('UTC')->format('H:i');
            }

            $availableTimes = array_diff($allowedTimes, $reservedTimes);

            $hasCurrentDate = false;
            $hasNextDay = false;

            foreach ($availableTimes as $time) {
                list($hours, $minutes) = explode(':', $time);

                $adjustedTime = $originalDate->copy()->setTime($hours, $minutes)->subHours(2)->setTimezone($timezone);

                $adjustedDate = $adjustedTime->format('Y-m-d');
                if ($adjustedTime->hour >= 0 && $adjustedTime->hour < 24) {
                    if (!in_array($adjustedDate, $allowedDates)) {
                        $allowedDates[] = $adjustedDate;
                    }
                    $hasCurrentDate = true;
                }

                if ($adjustedTime->hour < 0) {
                    $nextDayDate = $adjustedTime->copy()->addDay()->format('Y-m-d');
                    if (!in_array($nextDayDate, $allowedDates)) {
                        $allowedDates[] = $nextDayDate;
                    }
                    $hasNextDay = true;
                }
            }

            if (!$hasCurrentDate && $hasNextDay) {
                $allowedDates = array_diff($allowedDates, [$originalDate->setTimezone($timezone)->format('Y-m-d')]);
            }
        }

        if (empty($allowedDates)) {
            $allowedDates[] = $earliestDate;
        }

        return response()->json(['allowedDates' => array_values(array_unique($allowedDates))]);
    }

    public function getAllowedTimesTeacher(Request $request)
    {
        $request->validate([
            'date' => 'required|date',
        ]);

        $selectedDate = $request->input('date');

        $booking = Booking::where('date', $selectedDate)->first();

        if ($booking) {
            $allowedTimes = json_decode($booking->times, true);

            sort($allowedTimes);

            $agenda = Agenda::whereDate('start_time', $selectedDate)->get();

            $reservedTimes = [];
            foreach ($agenda as $event) {
                $reservedTimes[] = Carbon::parse($event->start_time)->format('H:i');
            }

            $allowedTimesWithReservation = [];
            foreach ($allowedTimes as $time) {
                $isReserved = in_array($time, $reservedTimes);
                $allowedTimesWithReservation[$time] = $isReserved;
            }

            return response()->json(['allowedTimes' => $allowedTimesWithReservation]);
        } else {
            return response()->json(['error' => 'No bookings available for the selected date'], 404);
        }
    }

    public function getAllowedTimes(Request $request)
    {
        $request->validate([
            'date' => 'required|date',
            'timezone' => 'required|string'
        ]);

        $selectedDate = $request->input('date');
        $timezone = $request->input('timezone');
        $currentDate = now()->format('Y-m-d');

        $bookings = Booking::where('date', $selectedDate)
            ->orWhere('date', Carbon::parse($selectedDate)->subDay()->format('Y-m-d'))
            ->get();

        $allowedTimes = [];

        foreach ($bookings as $booking) {
            $bookingTimes = json_decode($booking->times, true);

            $adjustedTimes = array_map(function($time) use ($booking, $timezone) {
                $utcTime = Carbon::createFromFormat('H:i', $time, 'UTC');
                $localTime = Carbon::parse($booking->date)
                    ->setTime($utcTime->hour, $utcTime->minute, $utcTime->second)
                    ->setTimezone($timezone)
                    ->subHours(2);
                return [
                    'date' => $localTime->format('Y-m-d'),
                    'time' => $localTime->format('H:i'),
                ];
            }, $bookingTimes);

            $agenda = Agenda::whereDate('start_time', $booking->date)->get();

            $reservedTimes = [];
            foreach ($agenda as $event) {
                $reservedTime = Carbon::parse($event->start_time)->setTimezone($timezone)->subHours(2);
                $reservedTimes[] = $reservedTime->format('H:i');
            }

            if ($selectedDate === $currentDate) {
                $currentTime = Carbon::now()->setTimezone($timezone)->subHours(2);
                $minAllowedTime = $currentTime->format('H:i');

                $adjustedTimes = array_filter($adjustedTimes, function($time) use ($minAllowedTime) {
                    return $time['time'] > $minAllowedTime;
                });
            }

            $adjustedTimes = array_filter($adjustedTimes, function($time) use ($reservedTimes) {
                return !in_array($time['time'], $reservedTimes);
            });

            foreach ($adjustedTimes as $time) {
                if ($time['date'] == $selectedDate) {
                    $allowedTimes[] = $time['time'];
                }
            }
        }

        if (!empty($allowedTimes)) {
            sort($allowedTimes);
            return response()->json(['allowedTimes' => array_values($allowedTimes)]);
        } else {
            return response()->json(['error' => 'No bookings available for the selected date'], 404);
        }
    }

    public function getAllEvents()
    {
        $agendas = Agenda::all();

        $events = [];

        foreach ($agendas as $agenda) {
            $startTime = is_string($agenda->start_time) ? Carbon::parse($agenda->start_time) : $agenda->start_time;
            $endTime = is_string($agenda->end_time) ? Carbon::parse($agenda->end_time) : $agenda->end_time;

            $events[] = [
                'title' => $agenda->name,
                'start' => $startTime->toDateTimeString(),
                'end' => $endTime->toDateTimeString(),
            ];
        }

        return response()->json($events);
    }

    public function deleteScheduledTime(Request $request, $date, $time)
    {

        $agendas = Agenda::whereDate('start_time', $date)->whereTime('start_time', $time)->get();
        foreach ($agendas as $agenda) {
            $user = $agenda->user;
            $name = $user->firstname . ' ' . $user->lastname;

            $this->sendRefundMessage($user, $date, $time);
            DeletedAgenda::create([
                'user_id' => $agenda->user_id,
                'name' => $name,
                'start_time' => $agenda->start_time,
                'end_time' => $agenda->end_time,
            ]);

            // Delete the event in Google Agenda
            $eventId = $agenda->google_event_id;
            $event = Event::find($eventId);
            $event->delete();

            $user->increment('credits');

            $agenda->delete();
        }

        $booking = Booking::where('date', $date)->first();
        if ($booking) {
            $allowedTimes = json_decode($booking->times, true);
            $key = array_search($time, $allowedTimes);
            if ($key !== false) {
                unset($allowedTimes[$key]);
                $booking->times = json_encode(array_values($allowedTimes));
                $booking->save();
                if (empty($allowedTimes)) {
                    $booking->delete();
                }
            }
        }

        return response()->json(['message' => 'Scheduled time(s) and booking deleted successfully'], 200);
    }

    public function deleteScheduledDate(Request $request, $date)
    {
        $agendas = Agenda::whereDate('start_time', $date)->get();
        foreach ($agendas as $agenda) {
            $user = $agenda->user;
            $user->increment('credits');
            $name = $user->firstname . ' ' . $user->lastname;

            $time = Carbon::parse($agenda->start_time)->format('H:i');
            $this->sendRefundMessage($user, $date, $time);

            DeletedAgenda::create([
                'user_id' => $agenda->user_id,
                'name' => $name,
                'start_time' => $agenda->start_time,
                'end_time' => $agenda->end_time,
            ]);

            $agenda->delete();
        }

        $booking = Booking::where('date', $date)->first();

        if ($booking) {
            $booking->delete();
            return response()->json(['message' => 'Scheduled date deleted successfully'], 200);
        } else {
            return response()->json(['error' => 'No booking found for the specified date'], 404);
        }
    }

    private function sendRefundMessage($user, $date, $time)
    {
        $adminRole = Role::where('role_name', 'admin')->first();
        $adminUser = $adminRole->users()->first();

        $formattedDate = Carbon::parse($date)->locale('en')->isoFormat('dddd, D MMMM YYYY');
        $formattedTime = Carbon::parse($time)->format('H:i');

        $messageBody = "Your appointment scheduled for $formattedDate at $formattedTime has been canceled. Your credits have been refunded.";

        $adminMessage = new ChMessage();
        $adminMessage->from_id = $adminUser->id;
        $adminMessage->to_id = $user->id;
        $adminMessage->body = $messageBody;

        $adminMessage->save();
    }

    private function sendUpdateMessage($user, $oldStartDate, $oldStartTime, $newStartDate, $newStartTime)
    {
        $adminRole = Role::where('role_name', 'admin')->first();
        $adminUser = $adminRole->users()->first();

        $formattedOldDate = Carbon::parse($oldStartDate)->locale('en')->isoFormat('dddd, D MMMM YYYY');
        $formattedOldTime = Carbon::parse($oldStartTime)->format('H:i');
        $formattedNewDate = Carbon::parse($newStartDate)->locale('en')->isoFormat('dddd, D MMMM YYYY');
        $formattedNewTime = Carbon::parse($newStartTime)->format('H:i');

        $messageBody = "Your appointment scheduled for $formattedOldDate at $formattedOldTime has been moved to $formattedNewDate at $formattedNewTime.";

        $adminMessage = new ChMessage();
        $adminMessage->from_id = $adminUser->id;
        $adminMessage->to_id = $user->id;
        $adminMessage->body = $messageBody;

        $adminMessage->save();
    }

    private function sendCreateMessage($user, $date, $time)
    {
        $adminRole = Role::where('role_name', 'admin')->first();
        $adminUser = $adminRole->users()->first();

        $formattedDate = Carbon::parse($date)->locale('en')->isoFormat('dddd, D MMMM YYYY');
        $formattedTime = Carbon::parse($time)->format('H:i');

        $messageBody = "An appointment for $formattedDate at $formattedTime has been scheduled. Your got debited of 1 credit.";

        $adminMessage = new ChMessage();
        $adminMessage->from_id = $adminUser->id;
        $adminMessage->to_id = $user->id;
        $adminMessage->body = $messageBody;

        $adminMessage->save();
    }

}
