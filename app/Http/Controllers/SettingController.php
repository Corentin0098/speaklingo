<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditProfileRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Models\Agenda;
use App\Models\ChMessage;
use App\Models\Country;
use App\Models\DeletedAgenda;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Chatify\Facades\ChatifyMessenger as Chatify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Purchase;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    public function setting() {

        $user = Auth::user();
        $purchases = Purchase::where('user_id', $user->id)->get();
        $agendas = Agenda::where('user_id', $user->id)->get();
        $deletedAgendas = DeletedAgenda::where('user_id', $user->id)->get();
        $messagesSent = ChMessage::where('from_id', $user->id)->get();
        $messagesReceived = ChMessage::where('to_id', $user->id)->get();
        $roles = UserRole::with('role')->where('user_id', $user->id)->get();
        $roleNames = $roles->map(function ($userRole) {
            return $userRole->role->role_name;
        });


        if (!$user) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        return view('setting', [
            'user' => $user,
            'purchases' => $purchases,
            'agendas' => $agendas,
            'deletedAgendas' => $deletedAgendas,
            'messagesSent' => $messagesSent,
            'messagesReceived' => $messagesReceived,
            'roles' => $roleNames,
        ]);
    }

    // UPDATE PROFILE
    public function updateProfile(EditProfileRequest $request)
    {
        $user = Auth::user();

        // Récupérer les données envoyées par le formulaire
        $formData = $request->only(['firstname', 'lastname', 'email', 'country_id', 'phone']);

        // Comparer les données du formulaire avec les données actuelles de l'utilisateur
        $updatedFields = array_filter($formData, function ($value, $key) use ($user) {
            return $user->$key !== $value;
        }, ARRAY_FILTER_USE_BOTH);

        // Mettre à jour les champs modifiés
        if (!empty($updatedFields)) {
            $user->update($updatedFields);
            return response()->json(['success' => true]);
        } else {
            // Aucun champ modifié
            return response()->json(['success' => false, 'message' => 'No changes detected']);
        }
    }

    public function updateProfilePhoto(Request $request)
    {
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            // Générer un nom de fichier unique
            $avatar = Str::uuid() . "." . $file->extension();

            // Enregistrer la photo de profil sur S3
            $path = Storage::disk('s3')->put('avatars', $file);

            // Mettre à jour le chemin de la photo de profil dans la base de données
            $update = User::where('id', Auth::user()->id)->update(['avatar' => $path]);

            $success = $update ? 1 : 0;
        }
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        if (Auth::check()) {
            $user = Auth::user();

            $validatedData = $request->validated();

            if ($validatedData['newPassword'] !== $validatedData['checkNewPassword']) {
                return response()->json(['error' => 'Passwords do not match.'], 422);
            }

            if (!Hash::check($validatedData['oldPassword'], $user->password)) {
                return response()->json(['error' => 'Old password is incorrect.'], 422);
            }

            $user->password = Hash::make($validatedData['newPassword']);
            $user->save();

            return response()->json(['success' => 'Password updated successfully.']);
        } else {
            return response()->json(['error' => 'User not authenticated.'], 401);
        }
    }
    public function verifyPassword(Request $request)
    {
        $password = $request->password;
        $hashedPassword = $request->hashedPassword;

        if (Hash::check($password, $hashedPassword)) {
            // Mot de passe correct
            return response()->json(['success' => true]);
        } else {
            // Mot de passe incorrect
            return response()->json(['success' => false]);
        }
    }
}
