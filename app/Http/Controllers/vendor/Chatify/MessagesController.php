<?php

namespace App\Http\Controllers\vendor\Chatify;

use App\Models\Country;
use DateTime;
use DateTimeZone;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use App\Models\User;
use App\Models\ChMessage as Message;
use App\Models\ChFavorite as Favorite;
use Chatify\Facades\ChatifyMessenger as Chatify;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
class MessagesController extends Controller
{
    protected $perPage = 30;

    /**
     * Authenticate the connection for pusher
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function pusherAuth(Request $request)
    {
        return Chatify::pusherAuth(
            $request->user(),
            Auth::user(),
            $request['channel_name'],
            $request['socket_id']
        );
    }

    /**
     * Returning the view of the app with the required data.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $roles = $user->roles;

        $fetch = User::where('id', $request['id'])->first();
        $messenger_color = Auth::user()->messenger_color;
        return view('Chatify::pages.app', [
            'id' => $id ?? 0,
            'messengerColor' => $messenger_color ? $messenger_color : Chatify::getFallbackColor(),
            'dark_mode' => Auth::user()->dark_mode < 1 ? 'light' : 'dark',
            'user' => $fetch,
            'roles' => $roles,
        ]);
    }

    /**
     * Fetch data (user, favorite.. etc).
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function idFetchData(Request $request)
    {
        $favorite = Chatify::inFavorite($request['id']);
        $fetch = User::where('id', $request['id'])->first();

        $userAvatar = null;
        $userCountry = null;
        $userTime = null;
        $gmtOffset = null;

        if ($fetch) {
            $userAvatar = Chatify::getUserWithAvatar($fetch)->avatar;

            $countryId = $fetch->country_id;
            if ($countryId) {
                $country = Country::find($countryId);
                if ($country) {
                    $userCountry = $country->country_name;

                    $countryTimezones = [
                        'Australia' => 'Australia/Sydney',
                        'Austria' => 'Europe/Vienna',
                        'Belgium' => 'Europe/Brussels',
                        'Brazil' => 'America/Sao_Paulo',
                        'Bulgaria' => 'Europe/Sofia',
                        'Canada' => 'America/Toronto',
                        'Croatia' => 'Europe/Zagreb',
                        'Cyprus' => 'Asia/Nicosia',
                        'Czech Republic' => 'Europe/Prague',
                        'Denmark' => 'Europe/Copenhagen',
                        'Estonia' => 'Europe/Tallinn',
                        'Finland' => 'Europe/Helsinki',
                        'France' => 'Europe/Paris',
                        'Germany' => 'Europe/Berlin',
                        'Ghana' => 'Africa/Accra',
                        'Gibraltar' => 'Europe/Gibraltar',
                        'Greece' => 'Europe/Athens',
                        'Hong Kong' => 'Asia/Hong_Kong',
                        'Hungary' => 'Europe/Budapest',
                        'India' => 'Asia/Kolkata',
                        'Indonesia' => 'Asia/Jakarta',
                        'Ireland' => 'Europe/Dublin',
                        'Italy' => 'Europe/Rome',
                        'Japan' => 'Asia/Tokyo',
                        'Kenya' => 'Africa/Nairobi',
                        'Latvia' => 'Europe/Riga',
                        'Liechtenstein' => 'Europe/Vaduz',
                        'Lithuania' => 'Europe/Vilnius',
                        'Luxembourg' => 'Europe/Luxembourg',
                        'Malaysia' => 'Asia/Kuala_Lumpur',
                        'Malta' => 'Europe/Malta',
                        'Mexico' => 'America/Mexico_City',
                        'Netherlands' => 'Europe/Amsterdam',
                        'New Zealand' => 'Pacific/Auckland',
                        'Nigeria' => 'Africa/Lagos',
                        'Norway' => 'Europe/Oslo',
                        'Poland' => 'Europe/Warsaw',
                        'Portugal' => 'Europe/Lisbon',
                        'Romania' => 'Europe/Bucharest',
                        'Singapore' => 'Asia/Singapore',
                        'Slovakia' => 'Europe/Bratislava',
                        'Slovenia' => 'Europe/Ljubljana',
                        'South Africa' => 'Africa/Johannesburg',
                        'Spain' => 'Europe/Madrid',
                        'Sweden' => 'Europe/Stockholm',
                        'Switzerland' => 'Europe/Zurich',
                        'Thailand' => 'Asia/Bangkok',
                        'United Arab Emirates' => 'Asia/Dubai',
                        'United Kingdom' => 'Europe/London',
                        'United States' => 'America/New_York'
                    ];

                    $countryTimezone = $countryTimezones[$userCountry] ?? 'UTC';
                    $date = new DateTime("now", new DateTimeZone($countryTimezone));

                    $gmtOffsetSeconds = $date->getOffset();

                    $hours = (int) ($gmtOffsetSeconds / 3600);
                    $minutes = (int) (($gmtOffsetSeconds % 3600) / 60);

                    if ($hours >= 0) {
                        $gmtOffset = sprintf("+%02d:%02d", $hours, $minutes);
                    } else {
                        $gmtOffset = sprintf("%02d:%02d", $hours, $minutes);
                    }

                    $userTime = $date->format('H:i A');
                }
            }
        }

        return Response::json([
            'favorite' => $favorite,
            'fetch' => $fetch,
            'user_avatar' => $userAvatar,
            'country' => $userCountry,
            'time' => $userTime,
            'gmt_offset' => $gmtOffset
        ]);
    }

    /**
     * This method to make a links for the attachments
     * to be downloadable.
     *
     * @param string $fileName
     * @return \Symfony\Component\HttpFoundation\StreamedResponse|void
     */
    public function download($fileName)
    {
        $filePath = config('chatify.attachments.folder') . '/' . $fileName;
        if (Chatify::storage()->exists($filePath)) {
            return Chatify::storage()->download($filePath);
        }
        return abort(404, "Sorry, File does not exist in our server or may have been deleted!");
    }

    /**
     * Send a message to database
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function send(Request $request)
    {
        // default variables
        $error = (object)[
            'status' => 0,
            'message' => null
        ];
        $attachment = null;
        $attachment_title = null;

        // if there is attachment [file]
        if ($request->hasFile('file')) {
            // allowed extensions
            $allowed_images = Chatify::getAllowedImages();
            $allowed_files  = Chatify::getAllowedFiles();
            $allowed        = array_merge($allowed_images, $allowed_files);

            $file = $request->file('file');
            // check file size
            if ($file->getSize() < Chatify::getMaxUploadSize()) {
                if (in_array(strtolower($file->extension()), $allowed)) {
                    // get attachment name
                    $attachment_title = $file->getClientOriginalName();
                    // upload attachment and store the new name
                    $attachment = Str::uuid() . "." . $file->extension();
                    $file->storeAs(config('chatify.attachments.folder'), $attachment, config('chatify.storage_disk_name'));
                } else {
                    $error->status = 1;
                    $error->message = "File extension not allowed!";
                }
            } else {
                $error->status = 1;
                $error->message = "File size you are trying to upload is too large!";
            }
        }

        if (!$error->status) {
            $message = Chatify::newMessage([
                'from_id' => Auth::user()->id,
                'to_id' => $request['id'],
                'body' => htmlentities(trim($request['message']), ENT_QUOTES, 'UTF-8'),
                'attachment' => ($attachment) ? json_encode((object)[
                    'new_name' => $attachment,
                    'old_name' => htmlentities(trim($attachment_title), ENT_QUOTES, 'UTF-8'),
                ]) : null,
            ]);
            $messageData = Chatify::parseMessage($message);
            if (Auth::user()->id != $request['id']) {
                Chatify::push("private-chatify.".$request['id'], 'messaging', [
                    'from_id' => Auth::user()->id,
                    'to_id' => $request['id'],
                    'message' => Chatify::messageCard($messageData, true)
                ]);
            }
        }

        // send the response
        return Response::json([
            'status' => '200',
            'error' => $error,
            'message' => Chatify::messageCard(@$messageData),
            'tempID' => $request['temporaryMsgId'],
        ]);
    }

    /**
     * fetch [user/group] messages from database
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function fetch(Request $request)
    {
        $query = Chatify::fetchMessagesQuery($request['id'])->latest();
        $messages = $query->paginate($request->per_page ?? $this->perPage);
        $totalMessages = $messages->total();
        $lastPage = $messages->lastPage();
        $response = [
            'total' => $totalMessages,
            'last_page' => $lastPage,
            'last_message_id' => collect($messages->items())->last()->id ?? null,
            'messages' => '',
        ];

        // if there is no messages yet.
        if ($totalMessages < 1) {
            $response['messages'] ='<p class="message-hint center-el"><span>Say \'hi\' and start messaging</span></p>';
            return Response::json($response);
        }
        if (count($messages->items()) < 1) {
            $response['messages'] = '';
            return Response::json($response);
        }
        $allMessages = null;
        foreach ($messages->reverse() as $message) {
            $allMessages .= Chatify::messageCard(
                Chatify::parseMessage($message)
            );
        }
        $response['messages'] = $allMessages;
        return Response::json($response);
    }

    /**
     * Make messages as seen
     *
     * @param Request $request
     * @return JsonResponse|void
     */
    public function seen(Request $request)
    {
        // make as seen
        $seen = Chatify::makeSeen($request['id']);
        // send the response
        return Response::json([
            'status' => $seen,
        ], 200);
    }

    /**
     * Get contacts list
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getContacts(Request $request)
    {
        $user = Auth::user();
        $contacts = [];

        // get all users that received/sent message from/to [Auth user]
        $users = Message::join('users', function ($join) {
            $join->on('ch_messages.from_id', '=', 'users.id')
                ->orOn('ch_messages.to_id', '=', 'users.id');
        })
            ->where(function ($q) {
                $q->where('ch_messages.from_id', Auth::user()->id)
                    ->orWhere('ch_messages.to_id', Auth::user()->id);
            })
            ->where('users.id', '!=', Auth::user()->id)
            ->select('users.id', 'users.avatar', 'users.firstname', 'users.lastname', DB::raw('MAX(ch_messages.created_at) max_created_at'))
            ->with('roles')
            ->orderBy('max_created_at', 'desc')
            ->groupBy('users.id', 'users.avatar', 'users.firstname', 'users.lastname')
            ->paginate($request->per_page ?? $this->perPage);

        $usersList = $users->items();

        if (count($usersList) > 0) {
            $contacts = '';
            foreach ($usersList as $user) {
                $contacts .= Chatify::getContactItem($user);
            }
        } else {
            $contacts = '<p class="message-hint center-el"><span>Your contact list is empty</span></p>';
        }

        return Response::json([
            'contacts' => $contacts,
            'total' => $users->total() ?? 0,
            'last_page' => $users->lastPage() ?? 1,
        ], 200);
    }

    /**
     * Update user's list item data
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateContactItem(Request $request)
    {
        // Get user data
        $user = User::where('id', $request['user_id'])->first();
        if(!$user){
            return Response::json([
                'message' => 'User not found!',
            ], 401);
        }
        $contactItem = Chatify::getContactItem($user);
        // send the response
        return Response::json([
            'contactItem' => $contactItem,
        ], 200);
    }

    /**
     * Put a user in the favorites list
     *
     * @param Request $request
     * @return JsonResponse|void
     */
    public function favorite(Request $request)
    {
        $userId = $request['user_id'];
        // check action [star/unstar]
        $favoriteStatus = Chatify::inFavorite($userId) ? 0 : 1;
        Chatify::makeInFavorite($userId, $favoriteStatus);

        // send the response
        return Response::json([
            'status' => @$favoriteStatus,
        ], 200);
    }

    /**
     * Get favorites list
     *
     * @param Request $request
     * @return JsonResponse|void
     */
    public function getFavorites(Request $request)
    {
        $favoritesList = null;
        $favorites = Favorite::where('user_id', Auth::user()->id)->get();

        foreach ($favorites as $favorite) {
            // get user data with roles
            $user = User::with('roles')->find($favorite->favorite_id);

            $favoritesList .= view('Chatify::layouts.favorite', [
                'user' => $user,
            ]);
        }

        // send the response
        return Response::json([
            'count' => $favorites->count(),
            'favorites' => $favorites->count() > 0
                ? $favoritesList
                : 0,
        ], 200);
    }

    /**
     * Search in messenger
     *
     * @param Request $request
     * @return JsonResponse|void
     */
    public function search(Request $request)
    {
        $getRecords = null;
        $input = trim(filter_var($request['input']));

        $userRole = Auth::user()->roles->first()->role_name;

        $user = Auth::user();
        $roles = $user->roles;

        $records = User::where('id','!=',Auth::user()->id)
                    ->where('firstname', 'LIKE', "%{$input}%")
                    ->when($userRole == 'student', function ($query) {
                        // Si l'utilisateur est un étudiant, limiter les résultats aux administrateurs
                        return $query->whereHas('roles', function ($roleQuery) {
                            $roleQuery->where('role_name', 'admin');
                        });
                    })
                    ->paginate($request->per_page ?? $this->perPage);
        foreach ($records->items() as $record) {
            $getRecords .= view('Chatify::layouts.listItem', [
                'get' => 'search_item',
                'user' => Chatify::getUserWithAvatar($record),
                'roles' => $roles
            ])->render();
        }
        if($records->total() < 1){
            $getRecords = '<p class="message-hint center-el"><span>Nothing to show.</span></p>';
        }
        // send the response
        return Response::json([
            'records' => $getRecords,
            'total' => $records->total(),
            'last_page' => $records->lastPage()
        ], 200);
    }

    /**
     * Get shared photos
     *
     * @param Request $request
     * @return JsonResponse|void
     */
    public function sharedPhotos(Request $request)
    {
        $shared = Chatify::getSharedPhotos($request['user_id']);
        $sharedPhotos = null;

        // shared with its template
        for ($i = 0; $i < count($shared); $i++) {
            $sharedPhotos .= view('Chatify::layouts.listItem', [
                'get' => 'sharedPhoto',
                'image' => Chatify::getAttachmentUrl($shared[$i]),
            ])->render();
        }
        // send the response
        return Response::json([
            'shared' => count($shared) > 0 ? $sharedPhotos : '<p class="message-hint"><span>Nothing shared yet</span></p>',
        ], 200);
    }

    /**
     * Delete conversation
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteConversation(Request $request)
    {
        // delete
        $delete = Chatify::deleteConversation($request['id']);

        // send the response
        return Response::json([
            'deleted' => $delete ? 1 : 0,
        ], 200);
    }

    /**
     * Delete message
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteMessage(Request $request)
    {
        // delete
        $delete = Chatify::deleteMessage($request['id']);

        // send the response
        return Response::json([
            'deleted' => $delete ? 1 : 0,
        ], 200);
    }

    public function updateSettings(Request $request)
    {
        $msg = null;
        $error = $success = 0;

        // dark mode
        if ($request['dark_mode']) {
            $request['dark_mode'] == "dark"
                ? User::where('id', Auth::user()->id)->update(['dark_mode' => 1])  // Make Dark
                : User::where('id', Auth::user()->id)->update(['dark_mode' => 0]); // Make Light
        }

        // If messenger color selected
        if ($request['messengerColor']) {
            $messenger_color = trim(filter_var($request['messengerColor']));
            User::where('id', Auth::user()->id)
                ->update(['messenger_color' => $messenger_color]);
        }
        // if there is a [file]
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            // Générer un nom de fichier unique
            $avatar = Str::uuid() . "." . $file->extension();

            // Enregistrer la photo de profil sur S3
            $path = Storage::disk('s3')->put('avatars', $file);

            // Mettre à jour le chemin de la photo de profil dans la base de données
            $update = User::where('id', Auth::user()->id)->update(['avatar' => $path]);

            $success = $update ? 1 : 0;
        }

        // send the response
        return Response::json([
            'status' => $success ? 1 : 0,
            'error' => $error ? 1 : 0,
            'message' => $error ? $msg : 0,
        ], 200);
    }

    /**
     * Set user's active status
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setActiveStatus(Request $request)
    {
        $activeStatus = $request['status'] > 0 ? 1 : 0;
        $status = User::where('id', Auth::user()->id)->update(['active_status' => $activeStatus]);
        return Response::json([
            'status' => $status,
        ], 200);
    }
}
