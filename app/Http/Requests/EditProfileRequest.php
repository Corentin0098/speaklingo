<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [];

        if ($this->filled('firstname')) {
            $rules['firstname'] = 'required|string|min:3|max:255';
        }

        if ($this->filled('lastname')) {
            $rules['lastname'] = 'required|string|min:3|max:255';
        }

        if ($this->filled('email')) {
            $rules['email'] = 'required|email|unique:users|max:255|regex:/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/|regex:/^[^@]+@\w+(\.\w+)+\w$/';
        }

        if ($this->filled('phone')) {
            $rules['phone'] = 'required|unique:users|numeric|digits_between:9,15';
        }

        if ($this->filled('country_id')) {
            $rules['country_id'] = 'required|exists:countries,country_id';
        }

        return $rules;
    }
}
