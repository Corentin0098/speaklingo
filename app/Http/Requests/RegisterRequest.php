<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'firstname' => 'required|string|min:3|max:255',
            'lastname' => 'required|string|min:3|max:255',
            'email' => 'required|email|unique:users|max:255|regex:/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/|regex:/^[^@]+@\w+(\.\w+)+\w$/',
            'phone' => 'required|unique:users|regex:/^\+\d{1,3}\d{9,14}$/',
            'country_id' => 'required|exists:countries,country_id',
            'password' => 'required|string|min:9|max:255|regex:/^(?=.*[A-Z])(?=.*\d).+$/',
            'terms' => 'required|accepted',
        ];
    }
}
