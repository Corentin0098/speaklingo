<p align="center">
  <img src="speaklingo-logo.png" alt="Logo du site Speaklingo" width="200" height="200">
</p> 

## Description

Dans le cadre de mon travaille de fin d'études, j'ai développé une plateforme web destinée à offrir des cours en ligne. Cette application vise à fournir un environnement virtuel où l'enseignante peut dispenser des cours et interagir avec les étudiants à distance.

La plateforme est développée en utilisant **Laravel**, **VueJs**, **Tailwind CSS**, **Chatify** et **Stripe**, avec une approche centrée sur l'utilisateur pour garantir une expérience d'apprentissage intuitive et efficace.

## Installation

1. **Les prérequis :**
    - PHP ^8.1
    - MySQL ^5.2.0
    - Composer ^2.6.4
    - npm ^10.5.0
    - Une connexion internet
    - Un serveur local (wamp, mamp,...)
    - Git bash
    - Un compte Gmail
    - Mot de passe de configuration Gmail SMTP
    - Clés de configuration AWS
    - Clés de configuration Pusher
    - Clés de configuration Stripe
    - Clés de configuration Exchange Rate
    - Clés de configuration Daily.co
      <br><br>
2. **Copie du projet :**

   2.1. Dans Git Bash ou le terminal sur Mac, allez jusqu'à votre dossier `www` ou `htdocs` puis effectuez la commande :
    ```bash
    git clone https://gitlab.com/Corentin0098/speaklingo
    ```

   2.2. Toujours via Git Bash/Terminal, allez dans le projet cloné (`cd speaklingo`) et effectuez séparément les commandes :
    ```bash
    composer install
    ```
    ```bash
    npm install
    ```
   (Ces installations peuvent prendre du temps sous Windows)

   2.3. Ensuite, effectuez la commande :
     ```bash
    php artisan install:project
     ``` 
   et répondez aux informations demandés.
   (le nom de la base de donnée et au choix . Ex : ‘speaklingo’).

   2.4. Effectuez la commande
    ```bash
    php artisan migrate
     ``` 

   2.5. Ils vous demanderont si vous voulez créer la base de données car elle n'existe pas, répondez **"yes"**.

   2.6. Ensuite, effectuez la commande :
    ```bash
    php artisan db:seed
    ``` 

   2.7. Créez l'administrateur via la commande (**FORTEMENT RECOMMANDÉ**, mettez **Frances Garnett**) :
    ```bash
   php artisan create:admin
    ``` 

   2.8. Si vous souhaitez créer des utilisateurs avec des échanges de messages entre l'administrateur et chaque utilisateur, exécutez la commande :
    ```bash
   php artisan create:users 10 5
    ``` 
   (10 = Nombre d'utilisateurs souhaités)

   (5 = Nombre des messages dans chaque conversation souhaité)

   Le mot de passe pour chacun des utilisateurs crées et : `password`

3. **configurez votre fichier .env :**

   3.1. Pour le SMTP de Gmail :
   ```bash
   MAIL_MAILER=smtp
   MAIL_HOST=smtp.gmail.com
   MAIL_PORT=587
   MAIL_USERNAME="votre adresse email"
   MAIL_PASSWORD="votre mot de passe d'application"
   MAIL_ENCRYPTION=tls
   MAIL_FROM_ADDRESS="votre adresse email"
   MAIL_FROM_NAME="${APP_NAME}"
   ``` 
   le mot de passe pour l'application est disponible à l'adresse si dessous:
   [Lien vers le mot de passe](https://myaccount.google.com/apppasswords)

   3.2. Pour AWS :
   ```bash
   AWS_ACCESS_KEY_ID="votre id AWS"
   AWS_SECRET_ACCESS_KEY="votre clé d'accès AWS"
   AWS_DEFAULT_REGION=eu-north-1
   AWS_BUCKET="votre bucket AWS"
   AWS_USE_PATH_STYLE_ENDPOINT=false
   ``` 
   les étapes pour récupérer les données nécessaires pour aws sont bien expliquées dans cette vidéo :
   [Lien vers la vidéo](https://www.youtube.com/watch?v=4zG5Nzxgq60)

   3.3. Pour Pusher :
   ```bash
   PUSHER_APP_ID="votre id Pusher"
   PUSHER_APP_KEY="votre clé Pusher"
   PUSHER_APP_SECRET="votre mot de passe Pusher"
   PUSHER_APP_CLUSTER=eu
   ```
   Pour récupérer les différentes données nécessaires, il suffit de se créer un compte sur Pusher et d'accéder au tableau de bord :
   [Lien vers Pusher](https://pusher.com/laravel/)

   3.4. Pour Stripe :
   ```bash
   STRIPE_SK="Votre clé Stripe SK"
   STRIPE_PK="Votre clé Stripe PK"
   VITE_EXCHANGE_RATE_API_KEY="Votre clé exchangerate"
   ```
   Pour récupérer les différentes données nécessaires, il suffit de se créer un compte sur Strip et d'accéder au tableau de bord :
    - [Lien vers Stripe](https://stripe.com/fr-be)
    - [Lien vers Exchange Rate](https://app.exchangerate-api.com)

   3.5. Pour Daily.co :
   ```bash
   VITE_DAILY_URL="votre url Daily.co"
   ```
   Pour récupérer l'url Daily.co, il suffit de se créer un compte et d'accéder au tableau de bord :
   [Lien vers Daily.co](https://www.daily.co/)

   3.6. Pour Google Calendar :
   ```bash
   GOOGLE_CALENDAR_ID="votre adresse Gmail"
   ```
4. **Démarrer les serveurs de l'application :**

   4.1. Si vous désirez accéder à l’application via le serveur Laravel vous pouvez effectuer la commande suivante :
   ```bash
   npm run speaklingo
   ``` 
   Vous pouvez ensuite vous rendre sur l’url fournie à coté de

   **INFO Server running on.**
   <br><br>
   L’installation est terminée.

## Structure de l'application

Ce diagramme représente la structure logique de mon application, montrant les différents composants, leurs relations et les flux de données entre eux.

![Diagram de l'architecture de l'application](<graph.png>)

### Composants principaux :

- **App** : Le composant principal de l'application.
- **Router** : Gère le routage des requêtes entrantes vers les contrôleurs appropriés.
- **Middleware** : Gère les requêtes HTTP avant qu'elles n'atteignent les contrôleurs, en appliquant diverses fonctions telles que l'authentification, la vérification CSRF, etc.
- **Controllers** : Les contrôleurs qui traitent les requêtes HTTP et manipulent les données en conséquence.
- **Models** : Représentent les données de l'application et définissent leur structure et leurs interactions.
- **Views** : Les vues qui affichent les données aux utilisateurs.
- **Public** : Contient les ressources statiques accessibles publiquement telles que les fichiers CSS, JavaScript, etc.
- **Resources** : Contient d'autres ressources utilisées par l'application telles que les fichiers de langue, les mises en page, etc.

### Relations principales :

- **App → Router → Controllers** : Le flux principal des requêtes HTTP dans l'application.
- **Controllers → Models** : Interaction entre les contrôleurs et les modèles pour récupérer, manipuler ou stocker des données.
- **Controllers → Views** : Les contrôleurs passent les données nécessaires aux vues pour les afficher aux utilisateurs.

Ce diagramme aide à visualiser l'architecture globale de mon application et à comprendre comment ses différents composants interagissent entre eux pour fournir les fonctionnalités nécessaires aux utilisateurs.

## Licence

Droits d'auteur 2024 **Corentin Paternottre**

Tous droits réservés. La reproduction, la distribution, la modification ou l'utilisation de ce logiciel, en tout ou en partie, sans l'autorisation expresse et écrite de l'auteur est strictement interdite.

Ce logiciel est fourni "tel quel", sans aucune garantie expresse ou implicite, y compris les garanties de qualité marchande, d'adéquation à un usage particulier et d'absence de contrefaçon. En aucun cas, l'auteur ne pourra être tenu responsable des dommages ou des réclamations découlant de l'utilisation de ce logiciel.
