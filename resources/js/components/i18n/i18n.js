import { createI18n } from 'vue-i18n'
import messagesEn from './languages/en.js'
import messagesFr from './languages/fr.js'

const i18n = createI18n({
    locale: 'en', // language by default
    messages: {
        en: messagesEn,
        fr: messagesFr,
    }
})

export default i18n
