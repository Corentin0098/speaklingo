import './bootstrap';
import '../css/tailwind.css';
import {createApp} from 'vue/dist/vue.esm-bundler.js';
import Toast, { POSITION } from "vue-toastification";
import "vue-toastification/dist/index.css";
import i18n from "./components/i18n/i18n.js";

import Vue3Typer from 'vue3-typer';
import "vue3-typer/dist/vue-typer.css"

import 'flag-icons/css/flag-icons.min.css';
import 'v-phone-input/dist/v-phone-input.css';
import { createVPhoneInput } from 'v-phone-input';

import VueCookieAcceptDecline from 'vue-cookie-accept-decline';
import 'vue-cookie-accept-decline/dist/vue-cookie-accept-decline.css';

// VUETIFY
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import '@mdi/font/css/materialdesignicons.css'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { fr } from 'vuetify/locale'

// HOME
import NavbarHome from "./components/user/unconnected/home/NavbarHome.vue";
import HomePage from "./components/user/unconnected/home/HomePage.vue";

// Page 404
import Page404 from "./components/Page404.vue";

// FORMS
// register
import NavbarRegister from "./components/user/unconnected/forms/register/NavbarRegister.vue";
import NavbarSuccessRegister from "./components/user/unconnected/forms/register/NavbarSuccessRegister.vue";
import RegisterComponent from "./components/user/unconnected/forms/register/RegisterComponent.vue";
import SuccessRegister from "./components/user/unconnected/forms/register/SuccessRegister.vue";
import ExpiredRegister from "./components/user/unconnected/forms/register/ExpiredRegister.vue";
// login
import NavbarLogin from "./components/user/unconnected/forms/login/NavbarLogin.vue";
import LoginComponent from "./components/user/unconnected/forms/login/LoginComponent.vue";
import ResetComponent from "./components/user/unconnected/forms/login/ResetComponent.vue";
import NewPasswordReset from "./components/user/unconnected/forms/login/NewPasswordReset.vue";

// PROFILE
import NavbarProfile from "./components/user/connected/profile/NavbarProfile.vue";
import ProfileComponent from "./components/user/connected/profile/ProfileComponent.vue";

// DASHBOARD
import DashboardComponent from "./components/admin/DashboardComponent.vue";

// SETTING
import SettingComponent from "./components/user/connected/setting/SettingComponent.vue";
import EditProfile from "./components/user/connected/setting/partial/EditProfile.vue";

// PAYMENT
import PaymentComponent from "./components/user/connected/payment/PaymentComponent.vue";
import SuccessComponent from "./components/user/connected/payment/SuccessComponent.vue";

// MEETING
import MeetingComponent from "./components/user/connected/meeting/MeetingComponent.vue";
import CalendarComponent from "./components/user/connected/meeting/CalendarComponent.vue";

// LESSON HISTORY
import LessonComponent from "./components/user/connected/lesson/LessonComponent.vue";

// CONSTANTE
const app = createApp();

const vuetify = createVuetify({
    components,
    directives,
    locale: {
        locale: 'fr',
        fallback: 'fr',
        messages: { fr },
    },
})

const vPhoneInput = createVPhoneInput({
    countryIconMode: 'svg',
    enableSearchingCountry: true
});



// HOME
app.component('NavbarHome', NavbarHome);
app.component('HomePage', HomePage);

// PageNotFound
app.component('Page404', Page404);

// FORMS
app.component('NavbarRegister', NavbarRegister);
app.component('NavbarSuccessRegister', NavbarSuccessRegister);
app.component('NavbarLogin', NavbarLogin);
app.component('RegisterComponent', RegisterComponent);
app.component('SuccessRegister', SuccessRegister);
app.component('ExpiredRegister', ExpiredRegister);
app.component('LoginComponent', LoginComponent);
app.component('ResetComponent', ResetComponent);
app.component('NewPasswordReset', NewPasswordReset);

// PROFILE
app.component('NavbarProfile', NavbarProfile);
app.component('ProfileComponent', ProfileComponent);

// DASHBOARD
app.component('DashboardComponent', DashboardComponent);

// SETTING
app.component('SettingComponent', SettingComponent);
app.component('EditProfile', EditProfile);

// PAYMENT
app.component('PaymentComponent', PaymentComponent);
app.component('SuccessComponent', SuccessComponent);

// MEETING
app.component('MeetingComponent', MeetingComponent);
app.component('CalendarComponent', CalendarComponent);

// LESSON HISTORY
app.component('LessonComponent', LessonComponent);

app.component('vue-cookie-accept-decline', VueCookieAcceptDecline);

app.use(i18n)
app.use(vPhoneInput);

app.use(Toast, {
    position: POSITION.TOP_CENTER,
    timeout: 4000
});
app.use(vuetify);
app.use(Vue3Typer);

app
    .mount('#app');
