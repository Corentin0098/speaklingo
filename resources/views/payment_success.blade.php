@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Paiement réussie';
        } else {
            document.title = 'Success payment';
        }
    </script>
@endsection

@section('title')
    Success payment
@endsection

@section('content')
    <success-component :home-route="'{{ route('home') }}'">
    </success-component>
@endsection
