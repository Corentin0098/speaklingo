<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ __('resetEmail.title') }}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            background-color: #f4f4f4;
            padding: 20px;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            background: #fff;
        }
        .logo {
            margin: auto;
            text-align: center;
        }
        .logo img {
            max-width: 100px;
        }
        .message {
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .message h2 {
            margin-top: 20px;
        }
        .button {
            display: inline-block;
            background-color: #149BFC;
            color: #fff;
            text-decoration: none;
            padding: 10px 20px;
            border-radius: 5px;
            margin-top: 15px;
            margin-bottom: 20px;
        }
        .button:hover {
            background-color: #2779bd;
        }
        .text {
            margin-top: 15px;
            margin-bottom: 20px;
        }
        .footer {
            border-top: 1px solid black;
            margin-top: 20px;
            margin-bottom: 20px;
            font-size: 14px;
            color: #555;
            padding-bottom: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="logo">
        <img src="{{ $message->embed(public_path('img/speaklingo-logo.png')) }}" alt="SpeakLingo Logo">
    </div>
    <div class="message">
        <h2>{{ __('resetEmail.heading') }}</h2>
        <p>{{ __('resetEmail.message') }}</p>
    </div>
    <div class="button">
        <a href="{{ $actionUrl }}" style="color: #fff; text-decoration: none;">{{ __('resetEmail.btn') }}</a>
    </div>
    <div class="text">
        <p>{{ __('resetEmail.text1') }}</p>
        <p>{{ __('resetEmail.text2') }}</p>
        <p>{{ __('resetEmail.sign') }}<br>SpeakLingo</p>
    </div>
    <div class="footer">
        <p>{{ __('resetEmail.footer1') }}</p>
        <p>{{ __('resetEmail.footer2') }}</p>
    </div>
</div>
</body>
</html>
