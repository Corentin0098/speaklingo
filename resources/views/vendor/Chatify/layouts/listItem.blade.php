{{-- -------------------- Contact list -------------------- --}}
@if($get == 'users' && !!$lastMessage)
<?php
$lastMessageBody = mb_convert_encoding($lastMessage->body, 'UTF-8', 'UTF-8');
$lastMessageBody = strlen($lastMessageBody) > 30 ? mb_substr($lastMessageBody, 0, 30, 'UTF-8').'..' : $lastMessageBody;
?>
<table class="messenger-list-item messenger-infoViewHead" data-contact="{{ $user->id }}">
    @foreach($user->roles as $role)
        <tr data-action="0">
            {{-- Avatar side --}}
            <td style="position: relative">
                @if($user->active_status)
                    <span class="activeStatus"></span>
                @endif
                <div class="avatar av-m" style="background-image: url({{ $role->role_name == 'admin' ? asset('img/avatar_du_professeur.png') : $user->avatar }});"></div>
            </td>
            {{-- center side --}}
            <td>
                <p data-id="{{ $user->id }}" data-type="user">
                    @if($role->role_name == 'admin')
                        Frances Garnett
                    @else
                        {{ strlen($user->firstname) > 12 ? trim(substr($user->firstname,0,12)).'..' : $user->firstname }}
                        {{ strlen($user->lastname) > 12 ? trim(substr($user->lastname,0,12)).'..' : $user->lastname }}
                    @endif
                    <span class="contact-item-time" data-time="{{$lastMessage->created_at}}">{{ $lastMessage->timeAgo }}</span>
                </p>
            <span>
                {{-- Last Message user indicator --}}
                {!!
                    $lastMessage->from_id == Auth::user()->id
                    ? '<span class="lastMessageIndicator">You :</span>'
                    : ''
                !!}
                {{-- Last message body --}}
                @if($lastMessage->attachment == null)
                {!!
                    $lastMessageBody
                !!}
                @else
                <span class="fas fa-file"></span> {{__('chatify.attachment')}}
                @endif
            </span>
            {{-- New messages counter --}}
                {!! $unseenCounter > 0 ? "<b>".$unseenCounter."</b>" : '' !!}
            </td>
        </tr>
    @endforeach
</table>
@endif

{{-- -------------------- Search Item -------------------- --}}
@if($get == 'search_item')
<table class="messenger-list-item" data-contact="{{ $user->id }}">
    <tr data-action="0">
        {{-- Avatar side --}}
        <td>
            @foreach($user->roles as $role)
                <div class="avatar av-m"
                     style="background-image: url({{ $role->role_name == 'admin' ? asset('img/avatar_du_professeur.png') : $user->avatar }});">
                </div>
            @endforeach
        </td>
        {{-- center side --}}
        <td>
            <p data-id="{{ $user->id }}" data-type="user">
            {{ strlen($user->firstname) > 12 ? trim(substr($user->firstname,0,12)).'..' : $user->firstname }}
            {{ strlen($user->lastname) > 12 ? trim(substr($user->lastname,0,12)).'..' : $user->lastname }}
        </td>
    </tr>
</table>
@endif
{{-- -------------------- Shared photos Item -------------------- --}}
@if($get == 'sharedPhoto')
<div class="shared-photo chat-image" style="background-image: url('{{ $image }}')"></div>
@endif


