{{-- user info and avatar --}}
<div class="messenger-infoView-btns">
    <a href="#" class="danger delete-conversation">{{__('chatify.deleteConversation')}}</a>
</div>
{{-- shared photos --}}
<div class="messenger-infoView-shared">
    <p class="messenger-title"><span>{{__('chatify.sharedPhotos')}}</span></p>
    <div class="shared-photos-list"></div>
</div>
