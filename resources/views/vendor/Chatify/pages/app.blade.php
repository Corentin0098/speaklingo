@include('Chatify::layouts.headLinks')
<div class="messenger">
    {{-- ----------------------Users/Groups lists side---------------------- --}}
    @foreach ($roles as $role)
            <div class="messenger-listView {{ !!$id ? 'conversation-active' : '' }}">
                {{-- Header and search bar --}}
                <div class="m-header">
                    <nav>
                        <a href="/"><i class="fas fa-chevron-left"></i> <span class="messenger-headTitle">{{__('chatify.home')}}</span> </a>
                        {{-- header buttons --}}
                        <nav class="m-header-right">
                            <a href="#"><i class="fas fa-cog settings-btn"></i></a>
                            <a href="#" class="listView-x"><i class="fas fa-times"></i></a>
                        </nav>
                    </nav>
                    {{-- Search input --}}
                    <input type="text" class="messenger-search" placeholder="{{__('chatify.search')}}" />
                    {{-- Tabs --}}
                    {{-- <div class="messenger-listView-tabs">
                        <a href="#" class="active-tab" data-view="users">
                            <span class="far fa-user"></span> Contacts</a>
                    </div>--}}

                </div>
                {{-- tabs and lists --}}
                <div class="m-body contacts-container pad-bottom-search">
                    {{-- Lists [Users/Group] --}}
                    {{-- ---------------- [ User Tab ] ---------------- --}}
                    <div class="show messenger-tab users-tab app-scroll" data-view="users">
                        {{-- Favorites --}}
                        <div class="favorites-section">
                            <div class="messenger-favorites app-scroll-hidden"></div>
                        </div>
                        {{-- Saved Messages --}}
                        {!! view('Chatify::layouts.listItem', ['get' => 'saved']) !!}
                        {{-- Contact --}}
                        <div class="listOfContacts" style="width: 100%;height: calc(100% - 272px);position: relative;"></div>
                    </div>
                    {{-- ---------------- [ Search Tab ] ---------------- --}}
                    <div class="messenger-tab search-tab app-scroll" data-view="search">
                        {{-- items --}}
                        <p class="messenger-title"><span>{{__('chatify.search')}}</span></p>
                        <div class="search-records">
                            <p class="message-hint center-el"><span>{{__('chatify.type_to_search')}}</span></p>
                        </div>
                    </div>
                </div>
            </div>
    @endforeach
    {{-- ----------------------Messaging side---------------------- --}}
    <div class="messenger-messagingView messenger-infoViewHead">
        {{-- header title [conversation name] amd buttons --}}
        <div class="m-header m-header-messaging">
            <nav class="chatify-d-flex chatify-justify-content-between chatify-align-items-center">
                {{-- header back button, avatar and user name --}}
                <div class="chatify-d-flex chatify-justify-content-between chatify-align-items-center">
                    <a href="#" class="show-listView"><i class="fas fa-arrow-left"></i></a>
                    <div style="margin: 0px 10px; margin-top: -5px; margin-bottom: -5px;">
                        @foreach ($roles as $role)
                            @if ($role->role_name == 'student')
                                <img class="avatar av-s header-avatar" src="{{ asset('img/avatar_du_professeur.png') }}" alt="Avatar du professeur">
                            @else
                                <div class="avatar av-s header-avatar" style="{{ $id ? '' : 'display: none;' }}" aria-description="Student Avatar"></div>
                            @endif
                        @endforeach
                    </div>
                    @foreach ($roles as $role)
                        @if ($role->role_name == 'student')
                            <p class="info-name">Frances Garnett</p>
                        @else
                            <p class="info-name bold">{{__('chatify.select_a_chat')}}</p>
                        @endif
                    @endforeach
                </div>
                {{-- header buttons --}}
                <nav class="m-header-right">
                    {{--  <a href="#" class="add-to-favorite"><i class="fas fa-star"></i></a> --}}
                    <a href="/"><i class="fas fa-home"></i></a>
                    <a href="#" class="show-infoSide"><i class="fas fa-info-circle"></i></a>
                </nav>
            </nav>
            {{-- Internet connection --}}
            <div class="internet-connection">
                <span class="ic-connected">{{__('chatify.connected')}}</span>
                <span class="ic-connecting">{{__('chatify.connecting')}}</span>
                <span class="ic-noInternet">{{__('chatify.no_internet_access')}}</span>
            </div>
        </div>

        {{-- Messaging area --}}
        <div class="m-body messages-container app-scroll">
            <div class="messages">
                <p class="message-hint center-el"><span>{{__('chatify.please_select_a_chat_to_start_messaging')}}</span></p>
            </div>
            {{-- Typing indicator --}}
            <div class="typing-indicator">
                <div class="message-card typing">
                    <div class="message">
                        <span class="typing-dots">
                            <span class="dot dot-1"></span>
                            <span class="dot dot-2"></span>
                            <span class="dot dot-3"></span>
                        </span>
                    </div>
                </div>
            </div>

        </div>
        {{-- Send Message Form --}}
        @include('Chatify::layouts.sendForm')
    </div>
    {{-- ---------------------- Info side ---------------------- --}}
    <div class="messenger-infoView app-scroll">
        {{-- nav actions --}}
        <nav>
            @foreach ($roles as $role)
                @if ($role->role_name == 'student')
                    <p>{{__('chatify.teacher_details')}}</p>
                @else
                    <p>{{__('chatify.student_details')}}</p>
                @endif
            @endforeach
            <a href="#"><i class="fas fa-times"></i></a>
        </nav>
        <div class="chatify-d-flex av-l">
            @foreach ($roles as $role)
                @if ($role->role_name == 'student')
                    <img class="avatar av-l" src="{{ asset('img/avatar_du_professeur.png') }}" alt="Avatar du professeur">
                @else
                    <div class="avatar av-l" style="{{ $id ? '' : 'display: none;' }}" aria-description="Student Avatar"></div>
                @endif
            @endforeach
        </div>
        @foreach ($roles as $role)
            @if ($role->role_name == 'student')
                <p>Frances Garnett</p>
            @else
                    <div class="center-text">
                        <p class="info-name bold">{{__('chatify.select_a_chat')}}</p>
                        <p class="info-lastname bold"></p>
                    </div>
            @endif

            <div class="messenger-infoView-btns">
                @if ($role->role_name == 'student')
                    <div>
                        <p class="messenger-title m-bottom"></p>
                        <p class="bold">{{__('chatify.tutor_time')}}</p>
                        <p id="london-time">{{__('chatify.loading')}}</p>
                        <p>{{__('chatify.londonGmt')}}</p>
                        <p class="messenger-title m-bottom"></p>
                    </div>
                @else
                    <div class="student-info-block" style="display: none;">
                        <p class="messenger-title m-bottom"></p>
                        <p class="bold">{{__('chatify.studentEmail')}}</p>
                        <p class="info-email"></p>
                        <p class="messenger-title m-bottom"></p>
                        <p class="bold">{{__('chatify.studentTime')}}</p>
                        <p class="info-time"></p>
                        <p>GMT <span class="info-gmt_offset"></span> / <span class="info-country"></span></p>
                        <p class="messenger-title m-bottom"></p>
                    </div>
                @endif
            @endforeach
            <a href="#" class="danger delete-conversation bottom-delete">{{__('chatify.deleteConversation')}}</a>
        </div>
    </div>
</div>

@include('Chatify::layouts.modals')
@include('Chatify::layouts.footerLinks')
