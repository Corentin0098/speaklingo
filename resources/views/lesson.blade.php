@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Mes cours';
        } else {
            document.title = 'My lessons';
        }
    </script>
@endsection

@section('title')
    My lessons
@endsection

@section('content')
    <navbar-profile :first-name="'{{ $user->firstname }}'"
                    :user-id="'{{ $user->id }}'"
                    :last-name="'{{ $user->lastname }}'"
                    :email="'{{ $user->email }}'"
                    :credits="'{{ $user->credits }}'"
                    :role='{{ json_encode($user->roles) }}'
                    :avatar="'{{ $user->avatar }}'"
                    :messages="{{ $messages }}"
                    :profile-route="'{{ route('profile') }}'"
                    :logout-route="'{{ route('logout') }}'"
                    :message-route="'{{ route('chat') }}'"
                    :lesson-route="'{{ route('lesson') }}'"
                    :setting-route="'{{ route('setting') }}'"
                    :payment-route="'{{ route('payment') }}'">
    </navbar-profile>

    <lesson-component :appointment="{{ $appointments }}"
                      :country="{{ json_encode($country) }}"
                      :home-route="'{{ route('home') }}'"
                      :message-route="'{{ route('chat') }}'"
                      :lesson-route="'{{ route('lesson') }}'"
                      :meeting-route="'{{ route('meeting') }}'">
    </lesson-component>


@endsection
