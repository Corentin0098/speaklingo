@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Connexion';
        } else {
            document.title = 'Login';
        }
    </script>
@endsection

@section('title')
    Login
@endsection

@section('content')

    <navbar-login :home-route="'{{ route('home') }}'"
                  :register-route="'{{ route('register') }}'">
    </navbar-login>

    <login-component :home-route="'{{ route('home') }}'"
                     :register-route="'{{ route('register') }}'"
                     :reset-route="'{{ route('reset') }}'"
                     :success="{{ session('email_verified_success', false) ? 'true' : 'false' }}"
                     :already-verified="{{ session('email_already_verified', false) ? 'true' : 'false' }}"
                     :error="{{ session('email_verify_error', false) ? 'true' : 'false' }}">
    </login-component>


@endsection
