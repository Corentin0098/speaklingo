@extends('layouts.master')

@section('title')
    Dashboard
@endsection

@section('content')

    <dashboard-component :first-name="'{{ $user->firstname }}'"
                  :role='{{ json_encode($user->roles) }}'
                  :users='{{ json_encode($users) }}'
                  :credits="'{{ $user->credits }}'"
                  :appointment="{{ $user->agenda }}"
                  :messages="{{ $messages }}"
                  :purchases="{{ $purchases }}"
                  :agendas="{{ $agendas }}"
                  :prices="{{ $prices }}"
                  :payment-route="'{{ route('payment') }}'"
                  :message-route="'{{ route('chat') }}'"
                  :meeting-route="'{{ route('meeting') }}'"
                  :logout-route="'{{ route('logout') }}'">
    </dashboard-component>
@endsection
