@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language;
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Inscription';
        } else {
            document.title = 'Register';
        }
    </script>
@endsection

@section('title')
    Register
@endsection

@section('content')

    <navbar-register :home-route="'{{ route('home') }}'"
                    :login-route="'{{ route('login') }}'">
    </navbar-register>

    <register-component :home-route="'{{ route('home') }}'"
                        :login-route="'{{ route('login') }}'">
    </register-component>

@endsection
