@extends('layouts.master')

@section('title')
    Speaklingo
@endsection

@section('content')
    <navbar-home :register-route="'{{ route('register') }}'"
                 :login-route="'{{ route('login') }}'">
    </navbar-home>
    <home-page  :register-route="'{{ route('register') }}'"
                :home-route="'{{ route('home') }}'"
                :prices="{{ $prices }}">
    </home-page>
@endsection
