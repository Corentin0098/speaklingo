@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Réinitialisation du mot de passe';
        } else {
            document.title = 'Reset password';
        }
    </script>
@endsection

@section('title')
    Reset password
@endsection

@section('content')

    <navbar-success-register :home-route="'{{ route('home') }}'"
                             :login-route="'{{ route('login') }}'">
    </navbar-success-register>

    <new-password-reset :home-route="'{{ route('home') }}'"
                     :register-route="'{{ route('register') }}'"
                     :login-route="'{{ route('login') }}'"
                     :reset-route="'{{ route('reset') }}'"
                     :success="{{ session('email_verified_success', false) ? 'true' : 'false' }}"
                     :already-verified="{{ session('email_already_verified', false) ? 'true' : 'false' }}"
                     :error="{{ session('email_verify_error', false) ? 'true' : 'false' }}">
    </new-password-reset>

@endsection
