@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Paramètres';
        } else {
            document.title = 'Settings';
        }
    </script>
@endsection

@section('title')
    Settings
@endsection

@section('content')
    <setting-component :user="'{{ $user }}'"
                       :first-name="'{{ $user->firstname }}'"
                       :last-name="'{{ $user->lastname }}'"
                       :email="'{{ $user->email }}'"
                       :country="'{{ $user->country->country_name }}'"
                       :password-user="'{{ $user->password }}'"
                       :phone="'{{ $user->phone }}'"
                       :avatar="'{{ $user->avatar }}'"
                       :purchases="{{ $purchases }}"
                       :agendas="{{ $agendas }}"
                       :deleted-agendas="{{ $deletedAgendas }}"
                       :messages-sent="{{ $messagesSent }}"
                       :messages-received="{{ $messagesReceived }}"
                       :roles="{{ $roles }}"
                       :logout-route="'{{ route('logout') }}'"
                       :profile-route="'{{ route('profile') }}'">
    </setting-component>
@endsection

