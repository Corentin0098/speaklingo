@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Mot de passe oublié';
        } else {
            document.title = 'Forgot password';
        }
    </script>
@endsection

@section('title')
    Forgot password
@endsection

@section('content')

    <navbar-login :home-route="'{{ route('home') }}'"
                  :register-route="'{{ route('register') }}'">
    </navbar-login>

    <reset-component :home-route="'{{ route('home') }}'"
                     :register-route="'{{ route('register') }}'"
                     :login-route="'{{ route('login') }}'"
                     :reset-route="'{{ route('reset') }}'"
                     :success="{{ session('email_verified_success', false) ? 'true' : 'false' }}"
                     :already-verified="{{ session('email_already_verified', false) ? 'true' : 'false' }}"
                     :error="{{ session('email_verify_error', false) ? 'true' : 'false' }}">
    </reset-component>

@endsection
