@extends('layouts.master')

@section('title')
    SpeakLingo
@endsection

@section('content')
    <navbar-success-register :home-route="'{{ route('home') }}'"
                             :login-route="'{{ route('login') }}'">
    </navbar-success-register>
    <expired-register :home-route="'{{ route('home') }}'">
    </expired-register>
@endsection
