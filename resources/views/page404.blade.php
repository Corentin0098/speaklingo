@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Page non trouvée';
        } else {
            document.title = 'Page not found';
        }
    </script>
@endsection

@section('title')
    Page not found
@endsection

@section('content')

    <Page404 :home-route="'{{ route('home') }}'">
    </Page404>

@endsection
