@extends('layouts.master')

@section('title')
    Profile
@endsection

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Profil';
        } else {
            document.title = 'Profile';
        }
    </script>
@endsection

@section('content')
    <navbar-profile :first-name="'{{ $user->firstname }}'"
                    :user-id="'{{ $user->id }}'"
                    :last-name="'{{ $user->lastname }}'"
                    :email="'{{ $user->email }}'"
                    :credits="'{{ $user->credits }}'"
                    :role='{{ json_encode($user->roles) }}'
                    :avatar="'{{ $user->avatar }}'"
                    :messages="{{ $messages }}"
                    :profile-route="'{{ route('profile') }}'"
                    :logout-route="'{{ route('logout') }}'"
                    :message-route="'{{ route('chat') }}'"
                    :lesson-route="'{{ route('lesson') }}'"
                    :setting-route="'{{ route('setting') }}'"
                    :payment-route="'{{ route('payment') }}'">
    </navbar-profile>

    <profile-component :first-name="'{{ $user->firstname }}'"
                       :credits="'{{ $user->credits }}'"
                       :appointment="{{ $user->agenda }}"
                       :country="{{ json_encode($country) }}"
                       :payment-route="'{{ route('payment') }}'"
                       :message-route="'{{ route('chat') }}'"
                       :lesson-route="'{{ route('lesson') }}'"
                       :meeting-route="'{{ route('meeting') }}'">
    </profile-component>
@endsection
