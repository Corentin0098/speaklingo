@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Paiement';
        } else {
            document.title = 'Payment';
        }
    </script>
@endsection

@section('title')
    Payment
@endsection

@section('content')
    <navbar-profile :first-name="'{{ $user->firstname }}'"
                    :user-id="'{{ $user->id }}'"
                    :last-name="'{{ $user->lastname }}'"
                    :email="'{{ $user->email }}'"
                    :credits="'{{ $user->credits }}'"
                    :role='{{ json_encode($user->roles) }}'
                    :avatar="'{{ $user->avatar }}'"
                    :messages="{{ $messages }}"
                    :profile-route="'{{ route('profile') }}'"
                    :logout-route="'{{ route('logout') }}'"
                    :message-route="'{{ route('chat') }}'"
                    :setting-route="'{{ route('setting') }}'"
                    :lesson-route="'{{ route('lesson') }}'"
                    :payment-route="'{{ route('payment') }}'">
    </navbar-profile>

    <payment-component :register-route="'{{ route('register') }}'"
                       :country="{{ json_encode($country) }}"
                       :prices="{{ $prices }}"
                       :lesson-route="'{{ route('lesson') }}'"
                       :message-route="'{{ route('chat') }}'">
    </payment-component>
@endsection
