@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Réunion';
        } else {
            document.title = 'Meeting';
        }
    </script>
@endsection

@section('title')
    Meeting
@endsection

@section('content')
    <meeting-component  :first-name="'{{ $user->firstname }}'"
                        :last-name="'{{ $user->lastname }}'"
                        :user-role="'{{ $role }}'"
                        :email="'{{ $user->email }}'">

    </meeting-component>
@endsection
