<!DOCTYPE html>
<html>

<head>
    <!-- Meta character set -->
    <meta charset="UTF-8">
    <!-- Meta viewport -->
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <!-- Meta description -->
    <meta id="meta-description" name="description" content="Envie d'améliorer votre anglais ? Optez pour des cours en ligne avec un professeur natif. Profitez de sessions individuelles avec un chat instantané, des visioconférences, un agenda intégré pour planifier vos cours et des paiements sécurisés via Stripe. Lancez-vous dès aujourd'hui !">
    <!-- Meta keywords -->
    <meta id="meta-keywords" name="keywords" content="cours d'anglais en ligne, apprendre l'anglais, professeur natif, cours particuliers d'anglais, chat instantané, visioconférence, agenda de cours, rendez-vous en ligne, paiements sécurisés, Stripe, anglais, langue anglaise, conversation en anglais, compétences linguistiques, expression orale, grammaire anglaise, vocabulaire anglais, compréhension écrite, compétences en lecture, préparation aux examens d'anglais, TOEFL, IELTS, Cambridge, améliorer l'anglais, pratiquer l'anglais, cours interactifs d'anglais en ligne, cours adaptés à votre niveau, maîtriser l'anglais, cours d'anglais pour débutants, cours d'anglais pour adultes, cours d'anglais pour enfants, cours d'anglais professionnels, étudier l'anglais, formation en anglais, enseignement de l'anglais, éducation en ligne, apprentissage en ligne, plateforme éducative, langue seconde, anglais comme langue étrangère">
    <!-- Token CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Site Logo -->
    <link rel="icon" href="{{ asset('img/speaklingo-favicon.png') }}" type="image/png"/>
    <!-- Site Title -->
    <title>@yield('title')</title>
    <!-- Tailwind -->
    @vite('resources/css/tailwind.css')
    <!-- CDN Icons Boostrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css" integrity="sha384-4LISF5TTJX/fLmGSxO53rV4miRxdg84mZsxmO8Rx5jGtp/LbrixFETvWa5a6sESd" crossorigin="anonymous">
    <!-- Font Roboto -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap">
    <script crossorigin src="https://unpkg.com/@daily-co/daily-js"></script>
    @yield('scripts')
</head>

<body class="bg-[#F8F9FF]">

    <div id="app">
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
    </div>

    <!-- Script JavaScript -->
    @vite('resources/js/app.js')
    <!-- Script JavaScript detection lang -->
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let currentLang = navigator.language;
            document.documentElement.lang = currentLang;
        });
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let currentLang = navigator.language;

            currentLang = currentLang.split('-')[0];

            let descriptions = {
                "fr": "Envie d'améliorer votre anglais ? Optez pour des cours en ligne avec un professeur natif. Profitez de sessions individuelles avec un chat instantané, des visioconférences, un agenda intégré pour planifier vos cours et des paiements sécurisés via Stripe. Lancez-vous dès aujourd'hui !",
                "en": "Ready to improve your English? Take online lessons with a native teacher. Benefit from one-on-one sessions with instant chat, video conferences, integrated scheduling to plan your classes, and secure payments via Stripe. Start today!",
                "es": "¿Listo para mejorar tu inglés? Toma clases en línea con un profesor nativo. Disfruta de sesiones individuales con chat instantáneo, videoconferencias, programación integrada para planificar tus clases y pagos seguros a través de Stripe. ¡Comienza hoy mismo!"
            };

            let metaDescription = document.getElementById('meta-description');
            if (descriptions[currentLang]) {
                metaDescription.setAttribute('content', descriptions[currentLang]);
            }
        });
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            function getKeywords() {
                return {
                    "fr": "cours d'anglais en ligne, apprendre l'anglais, professeur natif, cours particuliers d'anglais, chat instantané, visioconférence, agenda de cours, rendez-vous en ligne, paiements sécurisés, Stripe, anglais, langue anglaise, conversation en anglais, compétences linguistiques, expression orale, grammaire anglaise, vocabulaire anglais, compréhension écrite, compétences en lecture, préparation aux examens d'anglais, TOEFL, IELTS, Cambridge, améliorer l'anglais, pratiquer l'anglais, cours interactifs d'anglais en ligne, cours adaptés à votre niveau, maîtriser l'anglais, cours d'anglais pour débutants, cours d'anglais pour adultes, cours d'anglais pour enfants, cours d'anglais professionnels, étudier l'anglais, formation en anglais, enseignement de l'anglais, éducation en ligne, apprentissage en ligne, plateforme éducative, langue seconde, anglais comme langue étrangère",
                    "en": "online English courses, learn English, native teacher, private English lessons, instant chat, video conferencing, class schedule, online appointments, secure payments, Stripe, English language, English conversation, language skills, speaking skills, English grammar, English vocabulary, reading comprehension, English exams preparation, TOEFL, IELTS, Cambridge, improve English, practice English, interactive online English courses, tailored courses, master English, English for beginners, English for adults, English for kids, professional English courses, study English, English training, English teaching, online education, e-learning, educational platform, second language, English as a foreign language",
                    "es": "cursos de inglés en línea, aprender inglés, profesor nativo, clases particulares de inglés, chat instantáneo, videoconferencia, horario de clases, citas en línea, pagos seguros, Stripe, idioma inglés, conversación en inglés, habilidades lingüísticas, habilidades para hablar, gramática inglesa, vocabulario inglés, comprensión de lectura, preparación para exámenes de inglés, TOEFL, IELTS, Cambridge, mejorar el inglés, practicar inglés, cursos de inglés en línea interactivos, cursos adaptados, dominar el inglés, inglés para principiantes, inglés para adultos, inglés para niños, cursos de inglés profesionales, estudiar inglés, formación en inglés, enseñanza de inglés, educación en línea, aprendizaje en línea, plataforma educativa, segundo idioma, inglés como lengua extranjera"
                };
            }

            let currentLang = navigator.language;

            currentLang = currentLang.split('-')[0];

            let keywords = getKeywords();

            let metaKeywords = document.getElementById('meta-keywords');
            if (keywords[currentLang]) {
                metaKeywords.setAttribute('content', keywords[currentLang]);
            }
        });
    </script>
</body>
</html>
