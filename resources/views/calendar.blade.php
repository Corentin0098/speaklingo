@extends('layouts.master')

@section('scripts')
    <!-- Script JavaScript detection lang -->
    <script>
        let userLang = navigator.language
        userLang = userLang.split('-')[0];

        if (userLang === 'fr') {
            document.title = 'Calendrier';
        } else {
            document.title = 'Calendar';
        }
    </script>
@endsection

@section('title')
    Calendar
@endsection

@section('content')

    <calendar-component>
    </calendar-component>

@endsection
