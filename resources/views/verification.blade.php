<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ __('messages.title') }}</title>
    <style>
        .card {
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        img {
            width: 13%;
        }
        h1 {
            margin-top: 20px;
        }
        p {
            margin-top: 10px;
        }
        .p2 {
            margin-top: 60px;
        }
        .link {
            display: inline-block;
            margin-top: 20px;
            padding: 10px 20px;
            background-color: #149BFC;
            color: white;
            font-weight: bold;
            text-decoration: none;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div class="card">
    <img src="{{ $message->embed(public_path('img/speaklingo-logo.png')) }}" alt="SpeakLingo Logo">
    <p>{{ __('messages.thank_you') }}</p>
    <a href="{{ $verificationUrl }}" class="link">{{ __('messages.verify_email') }}</a>
    <p class="p2">{{ __('messages.contact_info') }}</p>
</div>
</body>
</html>
