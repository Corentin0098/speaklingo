<?php

return [
    'sk' => env('STRIPE_SK'), //Secret key
    'pk' => env('STRIPE_PK'), // Publish key
    'exchange_rate' => env('VITE_EXCHANGE_RATE_API_KEY')
];
