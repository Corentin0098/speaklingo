<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $countries = [
            ['country_name' => 'Australia', 'code_iso' => 'AU'],
            ['country_name' => 'Austria', 'code_iso' => 'AT'],
            ['country_name' => 'Belgium', 'code_iso' => 'BE'],
            ['country_name' => 'Brazil', 'code_iso' => 'BR'],
            ['country_name' => 'Bulgaria', 'code_iso' => 'BG'],
            ['country_name' => 'Canada', 'code_iso' => 'CA'],
            ['country_name' => 'Croatia', 'code_iso' => 'HR'],
            ['country_name' => 'Cyprus', 'code_iso' => 'CY'],
            ['country_name' => 'Czech Republic', 'code_iso' => 'CZ'],
            ['country_name' => 'Denmark', 'code_iso' => 'DK'],
            ['country_name' => 'Estonia', 'code_iso' => 'EE'],
            ['country_name' => 'Finland', 'code_iso' => 'FI'],
            ['country_name' => 'France', 'code_iso' => 'FR'],
            ['country_name' => 'Germany', 'code_iso' => 'DE'],
            ['country_name' => 'Ghana', 'code_iso' => 'GH'],
            ['country_name' => 'Gibraltar', 'code_iso' => 'GI'],
            ['country_name' => 'Greece', 'code_iso' => 'GR'],
            ['country_name' => 'Hong Kong', 'code_iso' => 'HK'],
            ['country_name' => 'Hungary', 'code_iso' => 'HU'],
            ['country_name' => 'India', 'code_iso' => 'IN'],
            ['country_name' => 'Indonesia', 'code_iso' => 'ID'],
            ['country_name' => 'Ireland', 'code_iso' => 'IE'],
            ['country_name' => 'Italy', 'code_iso' => 'IT'],
            ['country_name' => 'Japan', 'code_iso' => 'JP'],
            ['country_name' => 'Kenya', 'code_iso' => 'KE'],
            ['country_name' => 'Latvia', 'code_iso' => 'LV'],
            ['country_name' => 'Liechtenstein', 'code_iso' => 'LI'],
            ['country_name' => 'Lithuania', 'code_iso' => 'LT'],
            ['country_name' => 'Luxembourg', 'code_iso' => 'LU'],
            ['country_name' => 'Malaysia', 'code_iso' => 'MY'],
            ['country_name' => 'Malta', 'code_iso' => 'MT'],
            ['country_name' => 'Mexico', 'code_iso' => 'MX'],
            ['country_name' => 'Netherlands', 'code_iso' => 'NL'],
            ['country_name' => 'New Zealand', 'code_iso' => 'NZ'],
            ['country_name' => 'Nigeria', 'code_iso' => 'NG'],
            ['country_name' => 'Norway', 'code_iso' => 'NO'],
            ['country_name' => 'Poland', 'code_iso' => 'PL'],
            ['country_name' => 'Portugal', 'code_iso' => 'PT'],
            ['country_name' => 'Romania', 'code_iso' => 'RO'],
            ['country_name' => 'Singapore', 'code_iso' => 'SG'],
            ['country_name' => 'Slovakia', 'code_iso' => 'SK'],
            ['country_name' => 'Slovenia', 'code_iso' => 'SI'],
            ['country_name' => 'South Africa', 'code_iso' => 'ZA'],
            ['country_name' => 'Spain', 'code_iso' => 'ES'],
            ['country_name' => 'Sweden', 'code_iso' => 'SE'],
            ['country_name' => 'Switzerland', 'code_iso' => 'CH'],
            ['country_name' => 'Thailand', 'code_iso' => 'TH'],
            ['country_name' => 'United Arab Emirates', 'code_iso' => 'AE'],
            ['country_name' => 'United Kingdom', 'code_iso' => 'GB'],
            ['country_name' => 'United States', 'code_iso' => 'US']
        ];

        Country::insert($countries);
    }
}
