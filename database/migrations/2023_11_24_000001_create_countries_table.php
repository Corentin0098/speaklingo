<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('country_id')->comment('Primary key for the country');
            $table->string('country_name')->comment('Name of the country');
            $table->string('code_iso')->comment('ISO code of the country');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `countries` COMMENT = 'Table containing country information including name of country and ISO code'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('countries');
    }
};
