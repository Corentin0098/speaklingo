<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id()->comment('Primary key');
            $table->string('role_name')->default('student')->comment('Name of the role of the user, default is student');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `roles` COMMENT = 'Table containing different user roles (student or admin'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('roles');
    }
};
