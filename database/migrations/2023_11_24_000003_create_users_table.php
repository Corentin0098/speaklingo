<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id()->comment('Primary key');
            $table->string('lastname')->comment('Last name of the user');
            $table->string('firstname')->comment('First name of the user');
            $table->string('email')->unique()->comment('User\'s unique email address');
            $table->string('password')->comment('Hashed password of the user');
            $table->string('phone')->unique()->comment('User\'s unique phone number');
            $table->unsignedBigInteger('country_id')->comment('Foreign key referencing the country the user belongs to');
            $table->foreign('country_id')->references('country_id')->on('countries');
            $table->boolean('terms')->default(false)->comment('Indicates whether the user has accepted the terms and conditions');
            $table->unsignedInteger('credits')->default(0)->comment('Number of credits associated with the user');
            $table->dateTime('last_login')->nullable()->comment('Timestamp of the user\'s last login');
            $table->boolean('email_verified')->default(false)->comment('Indicates whether the user\'s email is verified');
            $table->string('verification_token')->nullable()->unique()->comment('Token used for email verification');
            $table->timestamp('verification_token_expires_at')->nullable()->comment('Timestamp indicating when the email verification token expires');
            $table->timestamps();

            $table->softDeletes()->comment('Allows to mark an user as deleted without physically deleting the data');
        });

        // Add a comment for the table
        DB::statement("ALTER TABLE `users` COMMENT = 'Table containing user information and settings'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
