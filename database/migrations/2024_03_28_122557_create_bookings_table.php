<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id()->comment('Primary key');
            $table->date('date')->comment('Date for which the availability is defined');
            $table->json('times')->nullable()->comment('Array containing available times for bookings on the specified date');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `bookings` COMMENT = 'Table containing available booking dates and times defined by the teacher (admin)'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bookings');
    }
};
