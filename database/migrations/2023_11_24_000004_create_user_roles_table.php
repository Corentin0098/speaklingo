<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_roles', function (Blueprint $table) {
            $table->id()->comment('Primary key');
            $table->unsignedBigInteger('user_id')->comment('Foreign key referencing users table');
            $table->foreign('user_id')->references('id')->on('users')->comment('Foreign key constraint to users table');
            $table->unsignedBigInteger('role_id')->comment('Foreign key referencing roles table');
            $table->foreign('role_id')->references('id')->on('roles')->comment('Foreign key constraint to roles table');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `user_roles` COMMENT = 'Table associating users with their roles'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_roles');
    }
};
