<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('deleted_agendas', function (Blueprint $table) {
            $table->id()->comment('Primary key');
            $table->unsignedBigInteger('user_id')->comment('ID of the user who who got is appointment deleted');
            $table->string('name')->comment('Name of the student who got is appointment deleted');
            $table->dateTime('start_time')->comment('Start time of the deleted agenda');
            $table->dateTime('end_time')->comment('End time of the deleted agenda');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->comment('Foreign key constraint linking to the users table, with cascading delete');
        });

        // Add a comment for the table
        DB::statement("ALTER TABLE `deleted_agendas` COMMENT = 'Table containing deleted appointments including details'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('deleted_agendas');
    }
};
