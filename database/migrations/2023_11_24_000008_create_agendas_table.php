<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->id()->comment('Primary key');
            $table->unsignedBigInteger('user_id')->comment('Foreign key referencing the ID of the user who took a appointment');
            $table->string('name')->comment('Name of the student who took a appointment');
            $table->dateTime('start_time')->comment('Start time of the agenda event');
            $table->dateTime('end_time')->comment('End time of the agenda event');
            $table->string('google_event_id')->nullable()->comment('Unique identifier for the Google Calendar event');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->comment('Foreign key constraint linking to the users table, with cascading delete');
        });

        DB::statement("ALTER TABLE `agendas` COMMENT = 'Table containing appointments made by users'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('agendas');
    }
};
