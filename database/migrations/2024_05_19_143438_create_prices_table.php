<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id()->comment('Primary key');
            $table->string('name')->comment('Name of the price (ex: Price 1, Price 2, Price 3)');
            $table->decimal('value', 8, 2)->comment('Value of the price, up to 8 digits with 2 decimal places');
            $table->timestamps();
        });

        // Insert initial data into the prices table
        DB::table('prices')->insert([
            ['name' => 'Price 1', 'value' => 15.00],
            ['name' => 'Price 2', 'value' => 30.00],
            ['name' => 'Price 3', 'value' => 60.00],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prices');
    }
};
