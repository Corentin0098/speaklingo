<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatifyMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ch_messages', function (Blueprint $table) {
            $table->uuid('id')->primary()->comment('Primary key');
            $table->bigInteger('from_id')->comment('ID of the user sending the message');
            $table->bigInteger('to_id')->comment('ID of the user receiving the message');
            $table->string('body', 5000)->nullable()->comment('Content of the message, can be text up to 5000 characters');
            $table->string('attachment')->nullable()->comment('File attachment associated with the message');
            $table->boolean('seen')->default(false)->comment('Flag indicating whether the message has been seen by the recipient');
            $table->timestamps();
        });

        // Add a comment for the table
        DB::statement("ALTER TABLE `ch_messages` COMMENT = 'Table containing chat messages between users'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch_messages');
    }
}
