<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id()->comment('Primary key');
            $table->unsignedBigInteger('user_id')->comment('Foreign key referencing the ID of the user who made the purchase');
            $table->string('transaction_id')->comment('Unique identifier for the transaction, used to track and reference the specific purchase in external payment systems');
            $table->string('amount')->comment('The total amount paid for the purchase');
            $table->string('currency')->comment('The currency in which the purchase amount was paid');
            $table->unsignedInteger('credits_earned')->default(0)->comment('The number of credits earned by the user from this purchase');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->comment('Foreign key constraint linking to the users table, with cascading delete');
        });

        DB::statement("ALTER TABLE `purchases` COMMENT = 'Table containing records of purchases made by users, including transaction details and credits earned'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchases');
    }
};
