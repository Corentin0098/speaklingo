<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('password_reset_tokens', function (Blueprint $table) {
            $table->string('email')->primary()->comment('User\'s email address for password reset');
            $table->string('token')->comment('Password reset token');
            $table->timestamp('created_at')->nullable()->comment('Timestamp when the password reset token was created');
        });

        DB::statement("ALTER TABLE `password_reset_tokens` COMMENT = 'Table containing password reset tokens for users'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('password_reset_tokens');
    }
};
