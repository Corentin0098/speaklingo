<?php


return [
    'title' => 'Vérifiez Votre Adresse Email',
    'thank_you' => 'Merci d\'avoir créé votre compte SpeakLingo. Veuillez vérifier votre adresse email pour commencer.',
    'verify_email' => 'Vérifiez votre adresse email',
    'contact_info' => 'Une fois votre adresse email vérifiée, vous pouvez continuer avec votre compte. Si vous avez des questions ou besoin d\'assistance, vous pouvez nous contacter à cette adresse : speaklingo1@gmail.com.',
];
