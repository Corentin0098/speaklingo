<?php

return [
    'title' => 'Réinitialisation de mot de passe',
    'heading' => 'Bonjour,',
    'message' => 'Vous recevez cet e-mail car nous avons reçu une demande de réinitialisation de mot de passe pour votre compte.',
    'btn' => 'Réinitialiser le mot de passe',
    'text1' => 'Ce lien de réinitialisation de mot de passe expirera dans 60 minutes.',
    'text2' => 'Si vous n\'avez pas demandé de réinitialisation de mot de passe, veuillez ignorer cet e-mail ou contacter le support.',
    'sign' => 'Cordialement,',
    'footer1' => 'Si vous avez des questions ou besoin d\'assistance, vous pouvez nous contacter à cette adresse : speaklingo1@gmail.com.',
    'footer2' => '© 2024 SpeakLingo. Tous droits réservés.',
];
