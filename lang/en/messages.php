<?php

return [
    'title' => 'Verify Your Email Address',
    'thank_you' => 'Thank you for creating your SpeakLingo account. Please verify your email address to get started.',
    'verify_email' => 'Verify your email address',
    'contact_info' => 'Once your email address is verified, you can proceed with your account. If you have questions or need assistance, you can contact us at this address: speaklingo1@gmail.com.',
];
