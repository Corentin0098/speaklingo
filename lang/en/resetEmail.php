<?php

return [
    'title' => 'Reset Password',
    'heading' => 'Hello,',
    'message' => 'You are receiving this email because we received a password reset request for your account.',
    'btn' => 'Reset Password',
    'text1' => 'This password reset link will expire in 60 minutes.',
    'text2' => 'If you did not request a password reset, no further action is required.',
    'sign' => 'Regards,',
    'footer1' => 'If you have questions or need assistance, you can contact us at this address: speaklingo1@gmail.com.',
    'footer2' => '© 2024 SpeakLingo. All rights reserved.',
];
