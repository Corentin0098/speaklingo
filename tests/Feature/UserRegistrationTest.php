<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use App\Models\User;

class UserRegistrationTest extends TestCase
{
    use WithFaker, DatabaseTransactions;

    public function test_user_registration_with_valid_data()
    {
        Mail::fake();

        $response = $this->followingRedirects()->post('createRegister', [
            'firstname' => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
            'password' => Hash::make($this->faker->password),
            'email' => $this->faker->unique()->safeEmail,
            'phone' => $this->faker->numerify('+###########'),
            'country_id' => 1,
            'terms' => true,
            'credits' => '0',
            'email_verified' => 0,
            'avatar' => 'avatars/avatar.jpg'
        ]);

        if ($response->status() === 500) {
            $this->fail("Registration failed with a 500 status code. check if your server is running");
        }

        if ($response->status() !== 500) {
            $response->assertStatus(200);
        }
    }


    public function test_user_registration_with_invalid_data()
    {
        $response = $this->post('createRegister', [
            'firstname' => 'jo', // Prénom non valide
            'lastname' => 'do', // Nom non valide
            'password' => 'azerty', // Mot de passe non valide
            'email' => 'invalid_email', // Adresse e-mail invalide
            'phone' => 'invalid_phone_number', // Numéro de téléphone invalide
            'country_id' => '123', // ID de pays non valide
            'terms' => false, // Termes non acceptés
        ]);

        // La redirection a échoué avec un statut de validation (422) ?
        $response->assertRedirect()
            // Vérification des erreurs de validation spécifiques
            ->assertSessionHasErrors(['firstname', 'lastname', 'password', 'email', 'phone', 'country_id', 'terms']);
    }
}
